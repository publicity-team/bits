# 2024 Audit of bits.d.o posts tags

The desire of this audit is to clean up and make uniform the meta tags 
(taglines) used on bits.d.o in order to make information easier to find and
manage for our readers and ourselves. In order to do so we must standardise
our tagging system.

This is the second part of the housekeeping task which established a new
directory structure toward the same goal of making items and information easier
to find as detailed in the updated top level README:
https://salsa.debian.org/publicity-team/bits/-/blob/master/README.md



### Goals: 
- Review, change, and update the majority of the tags presently used to make
news, articles, announcements, bits, and project information easier to find. 
- Establish a guideline for the publicity team and those submitting items for
publishing.
- Set standards that we can incorporate into our process.

## _This document is open for edit and suggestion_

Minor note: move *announce* tags to *news* tags reserving the announce tag only for items
that have been pushed to an #-announce list. 


# Existing Categories
- news 
- debconf
- distribution specific
- LTS
- Security
- Bits (project, teams, dpl)
- Debian Day
- Website
- Artwork
- Infrastructure
- Fundraising
- Audio
- Video
- Fonts
- Technical
- Announcements
- Calls
- Privacy
- Code Projects
- FLOSS
- Debian Developers
- Debian Contributors
- Publicity 
- $version
- Processors
- Debian Blends
- Community

## Proposed tags per category

### News 
- project
- bits
- dpn (Debian Project News)
- dpb (Debian Project Bits)
- news

### DebConf
- debconf$YY
- debconf
- teams
- Sponsor Posts: 
    -$company_name
    - sponsors
 

### Distribution Specific:
- versions
- hamm
- slink
- potato
- woody
- sarge
- etch
- lenny
- squeeze
- wheezy
- jessie
- stretch
- buster
- bullseye
- bookworm
- trixie
- testing
- stable
- unstable
- experimental

### LTS
- LTS (Lont Term support)
- support
- teams

### Security
- security
- updates
- teams 

### Bits from the DPL
- dpl
- bits from the dpl

### Debian Day
- anniversary
- birthday
- debianday

### Art and Media
- artwork
- imagination
- logo
- contest
- media 
- teams

### Fundraising
- donations
- fundraising
- SPI (Software in the Public Interest)

### Video
- peertube
- video
- teams 

### Audio
- audio

### Fonts
- fonts
- typography
- artwork

### Infrastructure 
- salsa
- software mirrors
- infrastructure
- technical
- qa
- policy
- sources
- reproducible builds
- website
- teams

### Technical
- zfs 
- extFS
- disk
- usrmerge

### Processors
- mips
- i386
- arduino

### Announcements
- statement
- resolution
- vote                                  
- DPL statement
- project
- debian-meeting
- election

### DebConf
- minidebconf
- debianevents
- cheese and wine party

### Requests for help
call for papers (was cfp)

### Diversity
- debian women
- diversity


### Privacy 
- tails
- privacy
- anonymity
- tor
- tor network
- onion services

### Code Projects
- gsoc
- outreachy
- project 
- fpga ?
- development
- diversity
- software

### FLOSS 
- free software
- FSFE (Free Software Foundation Europe)
- conservancy

### Contributors and Developers
- debian developers
- contributors
- sprint
- users
- meetDD
- contests


### Publicity
- publicity (team)
- press (team)
- announce
- interview
- meetDD

### Software
- perl
- ruby
- cloud
- kernel
- sip
- rtc
- xmpp
- jabber
- sfc

### Desktop Environment
- gnome
- X11
- plasma

### Debian Pure Blends
- Debian Accessibility
- Debian Astro
- DebiChem
- Debian Edu
- Debian Med
- Debian Multimedia
- Debian Science
- FreedomBox
- Hamradio
- teams

### Teams
- teams
- $TeamName

### In memorandum 
- postmortem

### Community
- community
- fosdem
- users
- forums 

## Need Categorization

- ada lovelace day
- election
- covid19
- thefuture
- pocketchip
- pyra
- zerophone
- Savoir-faire Linux
- openday
- patent trolls
- papercuts

