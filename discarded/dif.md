Title: New Section: Debian in the Field
Slug: debian-in-the-field
Date: 2015-11-15 09:00
Author: Laura Arjona Reina, Bernelle Verster
Tags: DiF, Debian in the Field, DebConf16
Status: draft

Debian in the Field illustrates some applications of Debian (and more 
widely, FLOSS) to a wide audience (from IT professionals or Debian 
members, to people or organizations whose only link to computers may 
be 'those things that do useful things').

The Debian website already has a ["Who uses 
Debian"](https://www.debian.org/users) section; here in the Debian blog 
we pretend to provide several more detailed experiences from those 
users.

These uses in the field are shared to show Debian in use and do not
express the view of nor is an endorsement from the Debian Project.

If you want to contribute articles in this "Debian in the Field" section, 
please visit our 
[wiki page about bits.debian.org](https://wiki.debian.org/Teams/Publicity/bits.debian.org).