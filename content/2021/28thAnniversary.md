Title: Debian celebrates our 28th Anniversary!
Slug: debianday-2021
Date: 2021-08-16 11:01
Author: Donald Norwood
Artist: Daniel Lenharo de Souza and Valessio Brito
Tags: debian, birthday
Status: published

![Debian28thCard](|static|/images/28thDebian_resize.png)

Today is Debian's 28th anniversary. We send all of our gratitude and love to
the many Contributors, Developers, and Users who have helped this vision and
project.

There are many celebrations of
[#DebianDay](https://wiki.debian.org/DebianDay/2021) happening around the
world, perhaps one is local to you? Later this month the celebration continues
with [#DebConf21](https://debconf21.debconf.org/) which will be held Online
during August 24 through August 28, 2021.
