Title: o Debian celebra seu 28º aniversário!
Slug: debianday-2021
Date: 2021-08-16 11:01
Author: Donald Norwood
Artist: Daniel Lenharo de Souza and Valessio Brito
Translator: Paulo Henrique de Lima Santana (phls)
Lang: pt-BR
Tags: debian, birthday
Status: published

![Debian28thCard](|static|/images/28thDebian_resize.png)

Hoje é o 28º aniversário do Debian. Enviamos toda a nossa gratidão e amor aos(às) muitos(as) colaboradores(as), desenvolvedores(as) e usuários(as) que tem ajudado nesta visão e projeto.

Existem muitas celebrações do [#DebianDay](https://wiki.debian.org/DebianDay/2021) acontecendo ao redor do mundo, talvez uma delas seja perto de você? No final deste mês, a celebração continua com a [#DebConf21](https://debconf21.debconf.org/), que será realizada online de 24 a 28 de agosto de 2021.
