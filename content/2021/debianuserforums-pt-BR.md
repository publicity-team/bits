Title: Alterações e atualizações do fórum de usuários(as) do Debian
Slug: debianuserforums
Date: 2021-08-11 13:00
Author: Donald Norwood
Tags: users, forums
Translator: Thiago Pezzo (tico)
Lang: pt-BR
Status: published

![Fórum de usuários(as) do Debian](|static|/images/DebianUserForumsIndexpage.png)

Diversas questões foram trazidas para a equipe de Comunidade do Debian a
respeito de responsividade, diagramação e atualizações necessárias de software
para o
[forums.debian.net](https://web.archive.org/web/20210627204458/http://forums.debian.net/).
Uma pergunta sempre era colocada:
[‘quem cuida do fórum?’](https://lists.debian.org/debian-project/2021/03/msg00046.html)

Durante as discussões, diversos(as) desenvolvedores(as) Debian se voluntariaram
para ajudar participando dos fóruns e auxiliando com as mudanças necessárias
para manter o serviço funcionando.

Estamos felizes em anunciar as seguintes modificações para o (NOVO!)
[forums.debian.net](https://forums.debian.net/), que resolveram a maior parte
das preocupações com responsabilidades, diagramação, usabilidade e
confiabilidade:

Os(As) desenvolvedores(as) Debian: Paulo Henrique de Lima Santana (phls), Felix
Lechner (lechner) e Donald Norwood (donald) foram adicionados(as) aos times de
Servidor e Administração do fórum.

A instância do servidor agora executa diretamente dentro da infraestrutura
Debian.

O software do fórum e da infraestrutura foi atualizado para versões mais
recentes (onde aplicável).

O DNS resolve tanto para IPv4 e IPv6.

SSL/HTTPS estão habilitados (estamos em 2021!).

Novos sistemas captcha e antispam estão atuando para bloquear spammers e bots,
e para tornar mais fácil o registro por humanos(as).

Novos(as) administradores(as) e moderadores(as) foram adicionados(as) para
fornecer cobertura extra durante mais horas e para combinar anos de experiência
com operações de fóruns e utilização do Debian.

Novos estilos de visualização estão disponíveis para a escolha dos(as)
usuários(as), alguns ideais para visualização de dispositivos móveis e tablets.

Sem querer, corrigimos o problema com tempo que o fórum anterior tinha de rodar
11 minutos mais rápido. :)

Esclarecemos os papéis e a visibilidade da equipe.

A responsividade para usuários(as) nos fóruns foi aumentada.

Os endereços de e-mail para moderadores(as)/administradores(as) foram
atualizados, verificados e validados, para acesso e respostas diretas.

As diretrizes para utilização do fórum por usuários(as) e equipe foram
[atualizadas](https://forums.debian.net/viewtopic.php?f=20&t=149781).

O Código de Conduta do Debian (CoC) foi transformado em um
[anúncio global](https://forums.debian.net/viewtopic.php?f=11&t=114009) como
acompanhamento para as diretrizes atualizadas recentemente, fornecendo aos(às)
moderadores(as)/administradores(as) um conjunto adicional de regras para lidar
com comportamentos indisciplinados e impróprios.

Algumas áreas de discussão foram renomeadas e reorientadas, junto com a
movimentação de várias discussões, tornando mais fácil a indexação e busca no
fórum.

Muitos (e novos!) recursos e extensões foram adicionados ao fórum para mais
uso facilitado e modernização, tais como um sistema de agradecimentos de
usuários(as) e pré-visualizações de discussões ao mover o ponteiro do mouse
sobre o tópico.

Também houve atualizações de algumas tarefas administrativas de servidor, as
quais não são próprias para uma lista pública, mas estamos cuidando disso
regularmente e com segurança. :)

Temos alguns detalhes pequenos aqui e ali que ainda necessitam de atenção, e
estamos trabalhando nisso.

Agradecemos imensamente à equipe de Administradores(as) de Sistema Debian (DSA)
e ao Ganeff, que reservaram um tempo para coordenar e auxiliar na instância do
fórum, DNS e detalhes de administração de rede e servidor; ao nosso prestativo
DPL Jonathan Carter; muitos agradecimentos para os(as) moderadores e
administradores(as), anteriores e atuais, do fórum: Mez, sunrat, 4D696B65,
arochester e cds60601, por ajudar com as modificações e com a transição; e
aos(às) usuários(as) do fórum que participaram em muitos ajustes. No fim das
contas, este foi um grande esforço comunitário, e todas e todos tiveram uma
parte significante nele. Obrigado(a)!
