Title: I love Free Software Day 2021: Show your love for Free Software
Slug: ilovefs-2021
Date: 2021-02-14 11:01
Author: Donald Norwood
Tags: contributing, debian, free software, FSFE
Status: published

[![ILoveFS banner](|static|/images/ilovefs-banner-medium-en.png)][ilovefs-link]

On this day February 14th, Debian joins the
[Free Software Foundation Europe][fsfe-link] in celebration of
["I Love Free Software" day][ilovefs-link]. This day takes the
time to appreciate and applaud all those who contribute to the many areas of
Free Software.

Debian sends all of our love and a giant “Thank you” to the upstream and
downstream creators and maintainers, hosting providers, partners, and of course
all of the Debian Developers and Contributors.

Thank you for all that you do in making Debian truly the Universal Operating
System and for keeping and making Free Software Free!

Send some love and show some appreciation for Free Software by spreading the
message and appreciation around the world, if you share in social
media the hashtag used is: #ilovefs.

[ilovefs-link]: https://fsfe.org/activities/ilovefs/index.en.html
[fsfe-link]: https://fsfe.org
