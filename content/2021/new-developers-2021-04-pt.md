Title: Novos(as) desenvolvedores(as) e mantenedores(as) Debian (março e abril de 2021)
Slug: new-developers-2021-04
Date: 2021-05-13 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Status: published

Os(As) seguintes colaboradores(as) do projeto se tornaram Desenvolvedores(as)
Debian nos últimos dois meses:

* Jeroen Ploemen (jcfp)
* Mark Hindley (leepen)
* Scarlett Moore (sgmoore)
* Baptiste Beauplat (lyknode)

Os(As) seguintes colaboradores(as) do projeto se tornaram Mantenedores(as)
Debian nos últimos dois meses:

* Gunnar Ingemar Hjalmarsson
* Stephan Lachnit

Parabéns a todos!
