Title: I love Free Software Day 2021 : montrez votre amour pour le logiciel libre !
Slug: ilovefs-2021
Date: 2021-02-14 18:01
Author: Donald Norwood
Lang: fr
Translator: Jean-Pierre Giraud
Tags: contributing, debian, free software, FSFE
Status: published

[![ILoveFS banner](|static|/images/ilovefs-banner-medium-en.png)][ilovefs-link]

Aujourd'hui, 14 février 2021, Debian se joint à la
[Free Software Foundation Europe][fsfe-link] pour la célébration de la
[journée « I Love Free Software »][ilovefs-link]. Cette journée est le moment
de remercier et d'applaudir tous ceux qui contribuent aux différents domaines
du logiciel libre.

Debian envoie tout son amour et un gigantesque « Merci » aux créateurs et
mainteneurs amont et aval, aux hébergeurs, à ses partenaires et bien sûr à
l'ensemble des développeurs et contributeurs de Debian.

Merci à vous pour tout ce que vous faites afin que Debian soit vraiment le
système d'exploitation universel et pour libérer et protéger la liberté du
logiciel libre !

Envoyez des témoignages de votre passion et de votre reconnaissance pour le
logiciel libre en diffusant ce message et vos appréciations à travers le
monde, et si vous les partagez sur les médias sociaux, utilisez le
*hashtag* #ilovefs.

[ilovefs-link]: https://fsfe.org/activities/ilovefs/index.en.html
[fsfe-link]: https://fsfe.org
