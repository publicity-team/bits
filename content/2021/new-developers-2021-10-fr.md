Title: Nouveaux développeurs et mainteneurs de Debian (septembre et octobre 2021)
Slug: new-developers-2021-10
Date: 2021-11-19 13:00
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Status: published

Les contributeurs suivants sont devenus développeurs Debian ces deux derniers
mois :

* Bastian Germann (bage)
* Gürkan Myczko (tar)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la même
période :

* Clay Stan
* Daniel Milde
* David da Silva Polverari
* Sunday Cletus Nkwuda
* Ma Aiguo
* Sakirnth Nagarasa

Félicitations !
