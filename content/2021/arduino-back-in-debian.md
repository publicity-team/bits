Title: Arduino is back on Debian
Slug: arduino-back-in-debian
Date: 2021-02-01 08:00
Author: Rock Storm
Tags: arduino, announce
Status: published

The Debian Electronics Team is happy to announce that the latest version of
Arduino, probably the most widespread platform for programming AVR
micro-controllers, is now [packaged and uploaded onto Debian unstable][1].

The last version of Arduino that was readily available in Debian was 1.0.5,
which dates back to 2013. It's been years of trying and failing but
finally, after a great months-long effort from Carsten Schoenert and Rock
Storm, we have got a working package for the latest Arduino. After over 7
years now, users will be able to install the Arduino IDE as easy as *"apt
install arduino"* again.

*"The purpose of this post is not just to announce this new upload but
actually more of a request for testing"* said Rock Storm. *" The title could
very well be _WANTED: Beta Testers for Arduino_ (dead or alive :P)."*. The
Debian Electronics Team would appreciate if anyone with the tools and
knowledge for it could give the package a try and let us know if he/she
finds any issues with it.

With this post we thank the Debian Electronics Team and all previous
contributors to the package. This feat would have not been achievable
without them.

[1]: https://packages.debian.org/unstable/arduino
