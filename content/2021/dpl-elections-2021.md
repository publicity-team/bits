Title: Debian Project Leader election 2021, Jonathan Carter re-elected.
Slug: 2021dpl-election
Date: 2021-04-18 10:00
Author: Donald Norwood
Tags: dpl, election, leader, vote
Status: published

The voting period and tally of votes for the Debian Project Leader election
has just concluded, and the winner is Jonathan Carter!

455 of 1,018 Developers voted using the
[Condorcet method](http://en.wikipedia.org/wiki/Condorcet_method).

More information about the results of the voting are available on the
[Debian Project Leader Elections 2021](https://www.debian.org/vote/2021/vote_001)
page.

Many thanks to Jonathan Carter and Sruthi Chandran for their campaigns, and to
our Developers for voting.
