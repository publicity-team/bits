Title: Happy 20th birthday Debian!
Date: 2013-08-16 00:05
Tags: birthday
Slug: 20-birthday-debian
Author: Ana Guerrero Lopez and Francesca Ciceri
Status: published

Today is Debian's 20 anniversary. This anniversary would have been impossible
without a strong community of users and developers. That's why for its
commemoration, we asked the Debian community what Debian meant to them. Below
you have a selection of the answers we got.

First:

> *Since I run Debian on my computers, I do not play anymore to 3D shooting
> games, not because of the lack of Free 3D drivers, but because developing
> Debian is more fun and addictive.*

Second:

> *Debian is a truly community based distro which is devoted to FOSS ideas and
> standards. It perfectly works on high variety of hardware. Users from all
> over the world have been using Debian and contributing to it during 20 years.
> And I am proud to be one of them. Happy Birthday, Debian!*

Third:

> *When I considered switching to Linux I asked friends which distribution to
> choose. I was told skip other distributions that were considered more
> newbie-friendly at the time and go straight to Debian instead. It might be
> more work initially, they said, but it would save me the hassle of switching
> to Debian later on, which I would inevitably do. Turns out they were right. I
> started my Linux experience more than ten years ago with Debian and have yet
> to see a better Linux distribution.*

Fourth:

> *[...] You are a worldwide community of volunteers working together for 20
> years now. To me this is an encouraging example and, given the sorry state of
> the world we live in, more important than the technical quality of the
> operating system. I wish you another successful twenty years and to stay as
> independent as you are.*

Fifth:

> *I initially started in Debian as it was an interesting technical challenge.
> Over the years the community and having good standards around what Free
> Software is has become more important.*

Sixth:

> *As a user of Debian for 14 years, and a former Debian Developer for 10, I
> would like to wish a happy birthday to the best project on the internet, and
> the best Linux distribution ever. Thanks for all of your support over the
> years!*

Seventh:

> *"Rock Solid Stability and Absolute Freedom" Thats what Debian means to me.*

Eighth:

> *Debian gives me a feeling that I am using the best linux has to offer. You
> know your machine is in safe hands.*

Ninth:

> *As a long time Debian user and sometimes supporter [...] I wish Debian all
> the best for next 20 years and beyond! Debian is the universal operating
> system. And it's free (and not just as in beer ;-)*

Tenth:

> *Debian is an awesome combination of obsessive high quality software and
> software freedom. It's a pleasure to be able to use and contribute to this
> project. Thanks to all for their excellent work! Cheers to 2\*\*20 years
> more!*

Eleventh:

> *[...] I am so very thankful to all of the people who have contributed to and
> continue to contribute to such a wonderful ecosystem of tools.   I love the
> commitment to the long term goals of security, freedom and transparency with
> respect to the computer systems that we all use and rely upon and the
> information that we store on them.*

Twelfth:

> *Debian is the operating system that makes me free.*

Thirteenth:

> *Debian is a family gathered around great idea. Its pure love.*
