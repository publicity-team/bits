Title: DPL elections 2019, congratulations Sam Hartman!
Date: 2019-04-21 14:00
Tags: dpl
Slug: results-dpl-elections-2019
Author: Ana Guerrero López
Status: published

The Debian Project Leader elections just finished and the winner is Sam
Hartman!

His term as project leader starts immediately today April 21st and expires on
April 20th 2020.

Of a total of 1003 developers, 378 developers voted using the
[Condorcet method](http://en.wikipedia.org/wiki/Condorcet_method).
More information about the result is available in the
[Debian Project Leader Elections 2019 page](https://www.debian.org/vote/2019/vote_001).

Many thanks to
[Joerg Jaspert](https://www.debian.org/vote/2019/platforms/joerg),
[Jonathan Carter](https://www.debian.org/vote/2019/platforms/jcc) and
[Martin Michlmayr](https://lists.debian.org/debian-vote/2019/03/msg00054.html)
for running.

And special thanks to Chris Lamb for his service as DPL during these last
twenty-four months!
