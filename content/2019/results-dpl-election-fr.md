Title: Élection du chef du projet Debian 2019, félicitations à Sam Hartman !
Date: 2019-04-21 14:00
Tags: dpl
Slug: results-dpl-elections-2019
Author: Ana Guerrero López
Lang: fr
Translator: Jean-Pierre Giraud
Status: published

L'élection du chef du projet Debian vient de s'achever et le vainqueur est
Sam Hartman !

Son mandat de chef du projet débute immédiatement aujourd'hui le 21 avril et
s'achèvera le 20 avril 2020.

Sur un total de 1003 développeurs, 378 ont voté avec la
[méthode Condorcet](https://fr.wikipedia.org/wiki/M%C3%A9thode_de_Condorcet).
Davantage d'informations sur le résultat sont disponibles sur la
[page de l'élection 2019 du chef du projet Debian](https://www.debian.org/vote/2019/vote_001).

Merci beaucoup à
[Joerg Jaspert](https://www.debian.org/vote/2019/platforms/joerg),
[Jonathan Carter](https://www.debian.org/vote/2019/platforms/jcc) et
[Martin Michlmayr](https://lists.debian.org/debian-vote/2019/03/msg00054.html)
pour s'être présentés.

Et un remerciement particulier pour Chris Lamb pour son rôle de chef du projet
Debian ces deux dernières années !
