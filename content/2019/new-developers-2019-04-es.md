Title: Nuevos desarrolladores y mantenedores de Debian (marzo y abril del 2019)
Slug: new-developers-2019-04
Date: 2019-05-11 16:35
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Status: published

Los siguientes colaboradores del proyecto se convirtieron en Debian Developers
en los dos últimos meses:

* Jean-Baptiste Favre (jbfavre)
* Andrius Merkys (merkys)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers
en los dos últimos meses:

* Christian Ehrhardt
* Aniol Marti
* Utkarsh Gupta
* Nicolas Schier
* Stewart Ferguson
* Hilmar Preusse

¡Felicidades a todos!
