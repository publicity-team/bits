Title: Nouveaux développeurs et mainteneurs de Debian (juillet et août 2019)
Slug: new-developers-2019-08
Date: 2019-09-17 17:30
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Status: published

Les contributeurs suivants sont devenus développeurs Debian ces deux derniers
mois :

* Keng-Yu Lin (kengyu)
* Judit Foglszinger (urbec)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la même
période :

* Hans van Kranenburg
* Scarlett Moore

Félicitations !
