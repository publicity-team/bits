Title: Những nhà Phát triển và Bảo trì Debian mới (Tháng giêng và hai 2019)
Slug: new-developers-2019-02
Date: 2019-03-12 13:00
Author: Jean-Pierre Giraud
Tags: project
Lang: vi
Translator: Trần Ngọc Quân
Status: published

Trong hai tháng qua, những người đóng góp sau đây nhận được tài khoản Phát triển Debian:

* Paulo Henrique de Lima Santana (phls)
* Unit 193 (unit193)
* Marcio de Souza Oliveira (marciosouza)
* Ross Vandegrift (rvandegrift)

Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là người Bảo trì Debian:

* Romain Perier
* Felix Yan

Xin chúc mừng!
