Title: Nouveaux développeurs et mainteneurs de Debian (septembre et octobre 2019)
Slug: new-developers-2019-10
Date: 2019-11-26 15:00
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Status: published

Les contributeurs suivants sont devenus développeurs Debian ces deux derniers
mois :

* Teus Benschop (teusbenschop)
* Nick Morrott (nickm)
* Ondřej Kobližek (kobla)
* Clément Hermann (nodens)
* Gordon Ball (chronitis)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la même
période :

* Nikos Tsipinakis
* Joan Lledó
* Baptiste Beauplat
* Jianfeng Li

Félicitations !
