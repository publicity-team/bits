Title: नए डेबियन डेवलपर्स और मेंटेनर्स (मार्च और अप्रैल 2023)
Slug: new-developers-2023-04
Date: 2023-05-25 12:00
Author: Jean-Pierre Giraud
Tags: project
Lang: hi-IN
Translator: Yashraj Moghe
Status: published

पिछले दो महीनों में निम्नलिखित सहायकों को उनका डेबियन डेवलपर अकाउंट मिला:

* James Lu (jlu)
* Hugh McMaster (hmc)
* Agathe Porte (gagath)

और निम्नलिखित को डेबियन मेंटेनर का स्थान दिया गया हैं:

* Soren Stoutner
* Matthijs Kooijman
* Vinay Keshava
* Jarrah Gosbell
* Carlos Henrique Lima Melara
* Cordell Bloor

इन्हें बधाईयाँ!
