Title: Nuevos desarrolladores y mantenedores de Debian (enero y febrero del 2023)
Slug: new-developers-2023-02
Date: 2023-03-22 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Status: published

Los siguientes colaboradores del proyecto se convirtieron en Debian Developers
en los dos últimos meses:

* Sahil Dhiman (sahil)
* Jakub Ružička (jru)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers
en los dos últimos meses:

* Josenilson Ferreira da Silva
* Ileana Dumitrescu
* Douglas Kosovic
* Israel Galadima
* Timothy Pearson
* Blake Lee
* Vasyl Gello
* Joachim Zobel
* Amin Bandali

¡Felicidades a todos!
