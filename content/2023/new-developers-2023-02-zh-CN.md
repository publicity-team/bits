Title: 新的 Debian 开发者和维护者 (2023年1月 至 2月)
Slug: new-developers-2023-02
Date: 2023-03-22 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: zh-CN
Translator: Boyuan Yang
Status: published

下列贡献者在过去的两个月中获得了他们的 Debian 开发者帐号：

* Sahil Dhiman (sahil)
* Jakub Ružička (jru)

下列贡献者在过去的两个月中被添加为 Debian 维护者：

* Josenilson Ferreira da Silva
* Ileana Dumitrescu
* Douglas Kosovic
* Israel Galadima
* Timothy Pearson
* Blake Lee
* Vasyl Gello
* Joachim Zobel
* Amin Bandali

祝贺他们！
