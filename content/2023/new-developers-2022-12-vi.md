Title: Những nhà Phát triển và Bảo trì Debian mới (Tháng mười một và mười hai 2022)
Slug: new-developers-2022-12
Date: 2023-01-24 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: vi
Translator: Trần Ngọc Quân
Status: published

Trong hai tháng qua, những người đóng góp sau đây nhận được tài khoản Phát triển Debian:

* Dennis Braun (snd)
* Raúl Benencia (rul)

Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là người Bảo trì Debian:

* Gioele Barabucci
* Agathe Porte
* Braulio Henrique Marques Souto
* Matthias Geiger
* Alper Nebi Yasak
* Fabian Grünbichler
* Lance Lin

Xin chúc mừng!
