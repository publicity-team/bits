Title: Novos(as) desenvolvedores(as) e mantenedores(as) Debian (março e abril de 2023)
Slug: new-developers-2023-04
Date: 2023-05-25 12:00
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Status: published

Os(As) seguintes colaboradores(as) do projeto se tornaram Desenvolvedores(as)
Debian nos últimos dois meses:

* James Lu (jlu)
* Hugh McMaster (hmc)
* Agathe Porte (gagath)

Os(As) seguintes colaboradores(as) do projeto se tornaram Mantenedores(as)
Debian nos últimos dois meses:

* Soren Stoutner
* Matthijs Kooijman
* Vinay Keshava
* Jarrah Gosbell
* Carlos Henrique Lima Melara
* Cordell Bloor

Parabéns a todos!
