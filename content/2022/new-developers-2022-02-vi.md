Title: Những nhà Phát triển và Bảo trì Debian mới (Tháng giêng và hai 2022)
Slug: new-developers-2022-02
Date: 2022-03-21 17:00
Author: Jean-Pierre Giraud
Tags: project
Lang: vi
Translator: Trần Ngọc Quân
Status: published

Trong hai tháng qua, những người đóng góp sau đây nhận được tài khoản Phát triển Debian:

* Francisco Vilmar Cardoso Ruviaro (vilmar)

Trong hai tháng qua, người đóng góp sau đây được thêm vào với tư cách là người Bảo trì Debian:

* Lu YaNing
* Mathias Gibbens
* Markus Blatt
* Peter Blackman
* David da Silva Polverari

Xin chúc mừng!
