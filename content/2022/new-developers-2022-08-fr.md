Title: Nouveaux développeurs et mainteneurs de Debian (juillet et août 2022)
Slug: new-developers-2022-08
Date: 2022-09-26 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Status: published

Les contributeurs suivants sont devenus développeurs Debian ces deux derniers
mois :

* Sakirnth Nagarasa (sakirnth)
* Philip Rinn (rinni)
* Arnaud Rebillout (arnaudr)
* Marcos Talau (talau)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la même
période :

* Xiao Sheng Wen
* Andrea Pappacoda
* Robin Jarry
* Ben Westover
* Michel Alexandre Salim

Félicitations !
