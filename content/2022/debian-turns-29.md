Title: Debian turns 29!
Date: 2022-08-16 13:00
Tags: debian, project, anniversary, DebianDay
Slug: debian-turns-29
Author: Laura Arjona Reina
Artist: Juliette Taka
Status: published

[![Alt Happy Birthday Debian by Juliette Taka - click to enlarge](|static|/images/happy_birthday_Debian_29_600x600.png)](https://bits.debian.org/images/happy_birthday_Debian_29.png)

Today is Debian's 29th anniversary. We recently
[wrote](https://bits.debian.org/2022/08/debianday2022-call-for-celebration.html)
about some ideas to celebrate the DebianDay,
and several events have been
[planned](https://wiki.debian.org/DebianDay/2022) in more than 14 locations.
You can join the party or organise something yourselves too!

Today is also an opportunity for you to start or resume your
contributions to Debian. For example, you can have a look at our
list of [Debian Teams](https://wiki.debian.org/Teams/),
install the
[how-can-i-help package](https://packages.debian.org/bullseye/how-can-i-help)
and see if there is a bug in any of the software that you use that you can help
to fix, start designing your artwork candidate for the next release,
contribute small tips on how to install Debian
on your machines to our wiki pages
[https://wiki.debian.org/InstallingDebianOn/](https://wiki.debian.org/InstallingDebianOn/),
or put a [Debian live](https://www.debian.org/CD/live/) image in an USB memory
and give it to some person near you, who still didn't discover Debian.

Our favorite operating system is the result of all the work we do together.
Thanks to everybody who has contributed in these 29 years, and happy birthday
Debian!
