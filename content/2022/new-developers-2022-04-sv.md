Title: Nya Debianutvecklare och Debian Maintainers (Mars och April 2022)
Slug: new-developers-2022-04
Date: 2022-05-13 17:00
Author: Jean-Pierre Giraud
Tags: project
Lang: sv
Translator: Andreas Rönnquist
Status: published

Följande bidragslämnare fick sina Debianutvecklarkonton under de senaste två
månaderna:

* Henry-Nicolas Tourneur (hntourne)
* Nick Black (dank)

Följande bidragslämnare adderades som Debian Maintainers under de senaste två
månaderna:

* Jan Mojžíš
* Philip Wyett
* Thomas Ward
* Fabio Fantoni
* Mohammed Bilal
* Guilherme de Paula Xavier Segundo

Grattis!
