Title: About
Slug: about
Lang: zh-CN
Translator: Boyuan Yang

本站是 [Debian][debian] 官方博客，作为以下各个服务的补充：

* [Debian 新闻发布](https://www.debian.org/News/)
* [Debian 计划新闻](https://www.debian.org/News/weekly/)
* [identi.ca 上的 Debian 帐号](https://identi.ca/debian)

本博客使用 [Pelican][pelican]。您可以在 [salsa.debian.org][gitdo]
站上找到当前博客使用的模板和主题。任何补丁、新主题提议和任何有建设性的建议都十分欢迎！

本站点内容使用与 [Debian][debian] 网站相同的许可证和授权发布，请见
 [Debian WWW 页面许可证][wwwlicense]。

如果您需要联系我们，请发电子邮件至 [debian-publicity
邮件列表][debian-publicity]。这是公开存档的列表。

[pelican]: http://getpelican.com/ "Find out about Pelican"
[debian]: https://www.debian.org "Debian -- 通用操作系统"
[gitdo]: https://salsa.debian.org/publicity-team/bits
[wwwlicense]: https://www.debian.org/license
[debian-publicity]: https://lists.debian.org/debian-publicity/
