Title: Bits from the DPL
Date: 2025-01-04
Tags: dpl, packages, salsa, continuous integration, sprint
Slug: bits-from-the-dpl-january
Author: Andreas Tille
Status: published

Dear Debian community,

this is bits from DPL for December.

Happy New Year 2025! Wishing everyone health, productivity, and a
successful Debian release later in this year.

Strict ownership of packages
============================

I'm glad my last bits sparked discussions about barriers between
packages and contributors, summarized temporarily in some post on the
[debian-devel list][op1]. As one participant aptly put it, we need a way
to visibly say, ["I'll do the job until someone else steps up"][op2].
Based on my experience with the Bug of the Day initiative, simplifying
the process for engaging with packages would significantly help.

Currently we have

1. [NMU][op3] The Developers Reference outlines several preconditions
    for NMUs, explicitly stating, "Fixing cosmetic issues or changing
    the packaging style in NMUs is discouraged." This makes NMUs
    unsuitable for addressing [package smells][op4]. However, I've seen
    NMUs used for tasks like switching to source format 3.0 or bumping
    the debhelper compat level. While it's technically possible to file
    a bug and then address it in an NMU, the process inherently limits
    the NMUer's flexibility to reduce package smells.

2. [Package Salvaging][op5] This is another approach for working on
    someone else's packages, aligning with the process we often follow
    in the Bug of the Day initiative. The criteria for [selecting packages][op6]
    typically indicate that the maintainer either lacks time to address
    open bugs, has lost interest, or is generally MIA.

Both options have drawbacks, so I'd welcome continued discussion on
criteria for lowering the barriers to moving packages to Salsa and
modernizing their packaging. These steps could enhance Debian overall
and are generally welcomed by active maintainers. The discussion also
[highlighted][op7] that packages on Salsa are often maintained
collaboratively, fostering the team-oriented atmosphere already
established in several [Debian teams][op8].

[op1]: https://lists.debian.org/debian-devel/2024/12/msg00480.html
[op2]: https://lists.debian.org/debian-devel/2024/12/msg00502.html
[op3]: https://www.debian.org/doc/manuals/developers-reference/pkgs.html#nmu
[op4]: https://trends.debian.net/packages-with-smells-sorted-by-maintainer.txt
[op5]: https://www.debian.org/doc/manuals/developers-reference/pkgs.en.html#package-salvaging
[op6]: https://salsa.debian.org/tille/tiny_qa_tools/-/wikis/Tiny-QA-tasks#bug-of-the-day
[op7]: https://lists.debian.org/debian-devel/2024/12/msg00479.html
[op8]: https://lists.debian.org/debian-devel/2024/12/msg00474.html

Salsa
=====

Continuous Integration
----------------------

As part of the ongoing discussion about package maintenance, I'm
considering the suggestion to switch from the current opt-in model for
Salsa CI to an [opt-out approach][sc1]. While I fully agree that human
verification is necessary when the [pipeline is activated][sc2], I
believe the current option to enable CI is less visible than it should
be. I'd welcome a more straightforward approach to improve access to
better testing for what we push to Salsa.

[sc1]: https://lists.debian.org/debian-devel/2024/12/msg00583.html
[sc2]: https://lists.debian.org/debian-devel/2024/12/msg00601.html

Number of packages not on Salsa
-------------------------------

In my campaign, [I stated][os1] that I aimed to reduce the number of
packages maintained outside Salsa to below 2,000. As of March 28, 2024,
the count was 2,368. As of this writing, the count stands at 1,928 [1],
so I consider this promise fulfilled. My thanks go out to everyone who
contributed to this effort. Moving forward, I'd like to set a more
ambitious goal for the remainder of my term and hope we can reduce the
number to below 1,800.

[os1]: https://lists.debian.org/debian-vote/2024/03/msg00057.html
[1] UDD query: SELECT DISTINCT count(\*) FROM sources WHERE release = 'sid' and vcs_url not like '%salsa%' ;

Past and future events
======================

Talk at MRI Together
--------------------

In early December, I gave a short [online talk][mt1], primarily
focusing on my work with the Debian Med team. I also used my position as
DPL to advocate for attracting more users and developers from the
scientific research community.

[mt1]: https://people.debian.org/\~tille/talks/20241205_mri_together_24/debian-science_mri_together_24_handout.pdf

FOSSASIA
--------

I originally planned to attend FOSDEM this year. However, given the
strong Debian presence there and the need for better representation at
the [FOSSASIA Summit][fa1], I decided to prioritize the latter. This
aligns with my goal of improving geographic diversity. I also look
forward to opportunities for inter-distribution discussions.

[fa1]: https://eventyay.com/e/4c0e0c27

Debian team sprints
-------------------

**Debian Ruby Sprint**

I approved the budget for the [Debian Ruby Sprint][sp1], scheduled for
January 2025 in Paris. If you're interested in contributing to the Ruby
team, whether in person or online, consider reaching out to them. I'm
sure any helping hand would be appreciated.

**Debian Med sprint**

There will also be a [Debian Med sprint][sp2] in Berlin in mid-February.
As usual, you don't need to be an expert in biology or
medicine–basic bug squashing skills are enough to contribute and enjoy
the friendly atmosphere the Debian Med team fosters at their sprints.
For those working in biology and medicine, we typically offer packaging
support. Anyone interested in spending a weekend focused on impactful
scientific work with Debian is warmly invited.

[sp1]: https://wiki.debian.org/Teams/Ruby/Meeting/Paris2025
[sp2]: https://wiki.debian.org/Sprints/2025/DebianMed

Again all the best for 2025

    Andreas.
