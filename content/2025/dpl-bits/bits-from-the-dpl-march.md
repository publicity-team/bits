Title: Bits from the DPL
Date: 2025-03-04
Tags: dpl, ftpmaster, Tiny tasks,
Slug: bits-from-the-dpl-march
Author: Andreas Tille
Status: published

Dear Debian community,

this is bits from DPL for February.

Ftpmaster team is seeking for new team members
--------

In December, Scott Kitterman announced his retirement from the project.
I personally regret this, as I vividly remember his invaluable support
during the Debian Med sprint at the start of the COVID-19 pandemic. He
even took time off to ensure new packages cleared the queue in under 24
hours. I want to take this opportunity to personally thank Scott for his
contributions during that sprint and for all his work in Debian.

With one fewer FTP assistant, I am concerned about the increased
workload on the remaining team. I encourage anyone in the Debian
community who is interested to consider reaching out to the FTP masters
about joining their team.

If you're wondering about the role of the [FTP masters][f1], I'd like to share
a fellow developer's perspective:

> "My read on the FTP masters is:
> > - In truth, they are the heart of the project.
> > - They know it.
> > - They do a fantastic job."

I fully agree and see it as part of my role as DPL to ensure this
remains true for Debian's future.

If you're looking for a way to support Debian in a critical role where
many developers will deeply appreciate your work, consider reaching out
to the team. It's a great opportunity for any Debian Developer to
contribute to a key part of the project.

[f1]: https://ftp-master.debian.org/

Project Status: Six Months of Bug of the Day
---------

In my Bits from the DPL talk at [DebConf24][1], I announced the Tiny Tasks
effort, which I intended to start with a [Bug of the Day][2] project.
Another idea was an Autopkgtest of the Day, but this has been postponed
due to limited time resources-I cannot run both projects in parallel.

The original goal was to provide small, time-bound examples for
newcomers. To put it bluntly: in terms of attracting new contributors,
it has been a failure so far. My offer to explain individual bug-fixing
commits in detail, if needed, received no response, and despite my
efforts to encourage questions, none were asked.

However, the project has several positive aspects: experienced
developers actively exchange ideas, collaborate on fixing bugs, assess
whether packages are worth fixing or should be removed, and work
together to find technical solutions for non-trivial problems.

So far, the project has been engaging and rewarding every day, bringing
new discoveries and challenges-not just technical, but also social.
Fortunately, in the vast majority of cases, I receive positive responses
and appreciation from maintainers. Even in the few instances where help
was declined, it was encouraging to see that in two cases, maintainers
used the ping as motivation to work on their packages themselves. This
reflects the dedication and high standards of maintainers, whose work is
essential to the project's success.

I once used the metaphor that this project is like wandering through a
dark basement with a lone flashlight-exploring aimlessly and discovering
a wide variety of things that have accumulated over the years.  Among
them are true marvels with popcon >10,000, ingenious tools, and
delightful games that I only recently learned about. There are also some
packages whose time may have come to an end-but each of them reflects
the dedication and effort of those who maintained them, and that
deserves the utmost respect.

Leaving aside the challenge of attracting newcomers, what have we
achieved since August 1st last year?

* Fixed more than one package per day, typically addressing multiple bugs.
* Added and corrected numerous Homepage fields and watch files.
* The most frequently patched issue was "Fails To Cross-Build From Source"
  (all including patches).
* Migrated several packages from cdbs/debhelper to dh.
* Rewrote many d/copyright files to DEP5 format and thoroughly reviewed them.
* Integrated all affected packages into Salsa and enabled Salsa CI.
* Approximately half of the packages were moved to appropriate teams,
  while the rest are maintained within the Debian or Salvage teams.
* Regularly performed team uploads, ITS, NMUs, or QA uploads.
* Filed several RoQA bugs to propose package removals where appropriate.
* Reported multiple maintainers to the MIA team when necessary.

With some goodwill, you can see a slight impact on the trends.debian.net
[graphs][3] (thank you Lucas for the graphs), but I would never claim that
this project alone is responsible for the progress. What I have also
observed is the steady stream of daily uploads to the [delayed queue][4],
demonstrating the continuous efforts of many contributors. This ongoing
work often remains unseen by most-including myself, if not for my
regular check-ins on this list. I would like to extend my sincere thanks
to everyone pushing fixes there, contributing to the overall quality and
progress of Debian's QA efforts.

If you examine the graphs for "Version Control System" and "VCS Hosting"
with the goodwill mentioned above, you might notice a positive trend
since mid-last year. The "Package Smells" category has also seen
reductions in several areas: "no git", "no DEP5 copyright", "compat <9",
and "not salsa". I'd also like to acknowledge the NMUers who have been
working hard to address the "format != 3.0" issue.  Thanks to all their
efforts, this specific issue never surfaced in the Bug of the Day
effort, but their contributions deserve recognition here.

The experience I gathered in this project taught me a lot and inspired
me to some followup we should discuss at a Sprint at DebCamp this year.

Finally, if any newcomer finds this information interesting, I'd be
happy to slow down and patiently explain individual steps as needed. All
it takes is asking questions on the [Matrix channel][5] to turn this into
a "teaching by example" session.

By the way, for newcomers who are interested, I used quite a few
abbreviations-all of which are explained in the [Debian Glossary][6].

[1]: https://debconf24.debconf.org/talks/20-bits-from-the-dpl/
[2]: https://salsa.debian.org/qa/tiny_qa_tools/-/wikis/Tiny-QA-tasks#bug-of-the-day
[3]: https://trends.debian.net/
[4]: https://ftp-master.debian.org/deferred.html
[5]: https://app.element.io/#/room/#debian-tiny-tasks:matrix.org
[6]: https://wiki.debian.org/Glossary

Sneak Peek at Upcoming Conferences
------------------

I will join two conferences in March-feel free to talk to me if you spot
me there.

1. FOSSASIA Summit 2025 (March 13-15, Bangkok, Thailand)
   Schedule: [https://eventyay.com/e/4c0e0c27/schedule][s1]

2. Chemnitzer Linux-Tage (March 22-23, Chemnitz, Germany)
   Schedule: [https://chemnitzer.linux-tage.de/2025/de/programm/vortraege][s2]

[s1]: https://eventyay.com/e/4c0e0c27/schedule
[s2]: https://chemnitzer.linux-tage.de/2025/de/programm/vortraege

Both events will have a Debian booth-come say hi!

Kind regards
    Andreas.
