Title: Bits from the DPL
Date: 2025-02-03
Tags: dpl, funding, code revue, X/Twitter
Slug: bits-from-the-dpl-february
Author: Andreas Tille
Status: published

Dear Debian community,

this is bits from DPL for January.

Sovereign Tech Agency
---------------------

I was recently pointed to Technologies and Projects supported by the
[Sovereign Tech Agency][st1] which is financed by the German Federal
Ministry for Economic Affairs and Climate Action. It is a subsidiary of
the Federal Agency for Disruptive Innovation, SPRIND GmbH.

It is worth sending applications there for distinct projects as that is
their preferred method of funding. Distinguished developers can also
apply for a fellowship position that pays up to 40hrs / week (32hrs when
freelancing) for a year. This is esp. open to maintainers of larger
numbers of packages in Debian (or any other Linux distribution).

There might be a chance that some of the Debian-related projects
submitted to the Google Summer of Code that did not get funded could be
retried with those foundations.  As per the [FAQ of the project][st2]:
"The Sovereign Tech Agency focuses on securing and strengthening open
and foundational digital technologies. These communities working on
these are distributed all around the world, so we work with people,
companies, and FOSS communities everywhere."

Similar funding organizations include the [Open Technology Fund][st3] and
[FLOSS/fund][st4]. If you have a Debian-related project that fits these
funding programs, they might be interesting options. This list is by no
means exhaustive—just some hints I’ve received and wanted to share. More
suggestions for such opportunities are welcome.

[st1]: https://www.sovereign.tech/tech
[st2]: https://www.sovereign.tech/faq#does-the-sovereign-tech-agency-invest-or-support-projects-outside-of-germany-or-the-european-union
[st3]: https://www.opentech.fund/
[st4]: https://floss.fund/

Year of code reviews
--------------------

On the debian-devel mailing list, there was a long thread titled
["Let's make 2025 a year when code reviews became common in Debian"][cr1].
It initially suggested something along the lines of:
["Let's review MRs in Salsa."][cr2]  The discussion quickly expanded to
include patches that have
been sitting in the BTS for years, which deserve at least the same
attention. One idea I'd like to emphasize is that associating BTS bugs
with MRs could be very convenient. It’s not only helpful for
documentation but also the easiest way to apply patches.

I’d like to emphasize that no matter what workflow we use—BTS, MRs, or a
mix—it is crucial to uphold Debian’s reputation for high quality.
However, this reputation is at risk as more and more old issues
accumulate. While Debian is known for its technical excellence,
long-standing bugs and orphaned packages remain a challenge. If we don’t
address these, we risk weakening the high standards that Debian is
valued for. Revisiting old issues and ensuring that unmaintained
packages receive attention is especially important as we prepare for the
[Trixie release][cr3].

[cr1]: https://lists.debian.org/debian-devel/2025/01/msg00267.html
[cr2]: https://lists.debian.org/debian-devel/2025/01/msg00457.html
[cr3]: https://lists.debian.org/debian-devel-announce/2025/01/msg00004.html

Debian Publicity Team will no longer post on X/Twitter
------------------------------------------------------

The Press Team has my full support in its decision to [stop posting on X][x1].
As per the [Publicity delegation][x2]:

* The team is in charge of deciding the most suitable publication
  venue or venues for announcements and when they are published.

the team once decided to join Twitter, but circumstances have since
changed. The current Press delegates have the institutional authority to
leave X, just as their predecessors had the authority to join. I
appreciate that the team carefully considered the matter, reinforced by
the arguments developed on the debian-publicity list, and communicated
its reasoning openly.

[x1]: https://micronews.debian.org/2025/1738154246.html
[x2]: https://lists.debian.org/debian-devel-announce/2023/11/msg00006.html

Kind regards,

Andreas.
