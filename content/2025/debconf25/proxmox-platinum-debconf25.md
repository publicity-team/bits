Title: Proxmox Platinum Sponsor of DebConf25
Slug: proxmox-platinum-debconf25
Date: 2025-02-06 11:50
Author: Sahil Dhiman
Artist: Proxmox
Tags: debconf25, debconf, sponsors, proxmox
Image: /images/proxmox.png
Status: published

[![proxmox-logo](|static|/images/proxmox.png)](https://www.proxmox.com/)

We are pleased to announce that **[Proxmox](https://www.proxmox.com)** has
committed to sponsor [DebConf25](https://debconf25.debconf.org/) as a
**Platinum Sponsor**.

Proxmox develops powerful, yet easy-to-use Open Source server software. The
product portfolio from Proxmox, including server virtualization, backup, and
email security, helps companies of any size, sector, or industry to simplify
their IT infrastructures. The Proxmox solutions are based on the great Debian
platform, and we are happy that we can give back to the community by sponsoring
DebConf25.

With this commitment as Platinum Sponsor, Proxmox is contributing to the Debian
annual Developers' conference, directly supporting the progress of Debian and
Free Software. Proxmox contributes to strengthen the community that
collaborates on Debian projects from all around the world throughout all of
the year.

Thank you very much, Proxmox, for your support of DebConf25!

## Become a sponsor too!

[DebConf25](https://debconf25.debconf.org/) will take place **from 14 to 20
July 2025 in Brest, France,** and will be preceded by DebCamp, from 7 to 13
July 2025.

DebConf25 is accepting sponsors! Interested companies and organizations may
contact the DebConf team through
[sponsors@debconf.org](mailto:sponsors@debconf.org), and visit the DebConf25
website at
[https://debconf25.debconf.org/sponsors /become-a-sponsor/](https://debconf25.debconf.org/sponsors/become-a-sponsor/).
