Title: DebConf25 Logo Contest Results
Slug: debconf25-logo-contest-results
Date: 2025-02-13 10:00
Author: Donald Norwood, Santiago Ruano Rincón, Jean–Pierre Giraud
Tags: debconf, debconf25, logos, artwork
Lang: en
Image: /images/juliana-dc25-logo1-vertical.png
Artist: Juliana Camargo
Status: published

Last November, the DebConf25 Team
[asked](https://lists.debian.org/debconf-announce/2024/11/msg00000.html)
the community to help design the logo for the 25th
[Debian Developers' Conference](https://debconf25.debconf.org) and the results
are in! The logo contest received
[23 submissions](https://wiki.debian.org/DebConf/25/Artwork/LogoProposals)
and we thank all the 295 people who took the time to participate in the
survey. There were several amazing proposals, so choosing was not easy.

We are pleased to
[announce](https://lists.debian.org/debconf-announce/2024/12/msg00000.html)
that the winner of the logo survey is '_Tower with red Debian Swirl originating
from blue water_' (option L), by Juliana Camargo and licensed CC BY-SA 4.0.

![[DebConf25 Logo Contest Winner]](|static|/images/juliana-dc25-logo1-vertical.png)

Juliana also shared with us a bit of her motivation, creative process and
inspiration when designing her logo:

> The idea for this logo came from the city's landscape, the place where the
> medieval tower looks over the river that meets the sea, almost like
> guarding it. The Debian red swirl comes out of the blue water splash as a
> continuous stroke, and they are also the French flag colours. I tried to
> combine elements from the city when I was sketching in the notebook,
> which is an important step for me as I feel that ideas flow much more
> easily, but the swirl + water with the tower was the most refreshing
> combination, so I jumped to the computer to design it properly. The water
> bit was the most difficult element, and I used the Debian swirl as a base
> for it, so both would look consistent. The city name font is a modern
> calligraphy style and the overall composition is not symmetric but balanced
> with the different elements. I am glad that the Debian community felt
> represented with this logo idea!

Congratulations, Juliana, and thank you very much for your contribution to
Debian!

The DebConf25 Team would like to take this opportunity to remind you that
DebConf, the annual international Debian Developers Conference, needs your
help. If you want to help with the DebConf 25 organization, don't hesitate to
reach out to us via the [#debconf-team](ircs://irc.oftc.net:6697/debconf-team)
channel on [OFTC](https://oftc.net/).

Furthermore, we are always looking for sponsors. DebConf is run on a
non-profit basis, and all financial contributions allow us to bring together
a large number of contributors from all over the globe to
work collectively on Debian. Detailed information about the
[sponsorship opportunities](https://debconf25.debconf.org/sponsors/become-a-sponsor/)
is available on the DebConf 25 website.

See you in Brest!
