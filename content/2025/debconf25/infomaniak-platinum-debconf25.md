Title: Infomaniak Platinum Sponsor of DebConf25
Slug: infomaniak-platinum-debconf25
Date: 2025-01-25 11:22
Author: Sahil Dhiman
Artist: Infomaniak
Tags: debconf25, debconf, sponsors, infomaniak
Image: /images/infomaniak.png
Status: published

[![infomaniaklogo](|static|/images/infomaniak.png)](https://www.infomaniak.com/)

We are pleased to announce that
**[Infomaniak](https://www.infomaniak.com)** has committed to sponsor
[DebConf25](https://debconf25.debconf.org/) as a **Platinum Sponsor**.

Infomaniak is Switzerland’s leading developer of Web technologies. With
operations all over Europe and based exclusively in Switzerland, the company
designs and manages its own data centers powered by 100% renewable energy,
and develops all its solutions locally, without outsourcing. With millions of
users and the trust of public and private organizations across Europe - such
as RTBF, the United Nations, central banks, over 3,000 radio and TV stations,
as well as numerous cities and security bodies - Infomaniak stands for
sovereign, sustainable and independent digital technology. The company offers
a complete suite of collaborative tools, cloud hosting, streaming, marketing
and events solutions, while being owned by its employees and self-financed
exclusively by its customers.

With this commitment as Platinum Sponsor, Infomaniak is contributing to
the Debian annual Developers' conference, directly supporting the
progress of Debian and Free Software. Infomaniak contributes to
strengthen the community that collaborates on Debian projects from all
around the world throughout all of the year.

Thank you very much, Infomaniak, for your support of DebConf25!

## Become a sponsor too!

[DebConf25](https://debconf25.debconf.org/) will take place from
**14th to July 20th 2025 in Brest, France,** and will be preceded by DebCamp,
from 7th to 13th July 2025.

DebConf25 is accepting sponsors! Interested companies and organizations should
contact the DebConf team through
[sponsors@debconf.org](mailto:sponsors@debconf.org), or visit the DebConf25
website at
[https://debconf25.debconf.org/sponsors/become-a-sponsor/](https://debconf25.debconf.org/sponsors/become-a-sponsor/).
