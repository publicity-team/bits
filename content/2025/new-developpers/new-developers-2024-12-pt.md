Title: Novos(as) desenvolvedores(as) e mantenedores(as) Debian (novembro e dezembro de 2024)
Slug: new-developers-2024-12
Date: 2025-01-28 20:00
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Translator:
Status: published

Os(As) seguintes colaboradores(as) do projeto se tornaram Desenvolvedores(as)
Debian nos últimos dois meses:

* Ananthu C V (weepingclown)
* Andrea Pappacoda (tachi)
* Athos Coimbra Ribeiro (athos)
* Gioele Barabucci (gioele)
* Jongmin Kim (jmkim)
* Shengqi Chen (harry)
* Frans Spiesschaert (frans)

Os(As) seguintes colaboradores(as) do projeto se tornaram Mantenedores(as)
Debian nos últimos dois meses:

* Tianyu Chen
* Emmanuel FARHI
* наб
* Nicolas Schodet

Parabéns a todos!
