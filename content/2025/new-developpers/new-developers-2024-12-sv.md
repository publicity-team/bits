Title: Nya Debianutvecklare och Debian Maintainers (November och December 2024)
Slug: new-developers-2024-12
Date: 2025-01-28 20:00
Author: Jean-Pierre Giraud
Tags: project
Lang: sv
Translator: Andreas Rönnquist
Status: published

Följande bidragslämnare fick sina Debianutvecklarkonton under de senaste två
månaderna:

* Ananthu C V (weepingclown)
* Andrea Pappacoda (tachi)
* Athos Coimbra Ribeiro (athos)
* Gioele Barabucci (gioele)
* Jongmin Kim (jmkim)
* Shengqi Chen (harry)
* Frans Spiesschaert (frans)

Följande bidragslämnare adderades som Debian Maintainers under de senaste två
månaderna:

* Tianyu Chen
* Emmanuel FARHI
* наб
* Nicolas Schodet

Grattis!
