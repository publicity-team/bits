Title: Những nhà Phát triển và Bảo trì Debian mới (Tháng mười một và mười hai 2024)
Slug: new-developers-2024-12
Date: 2025-01-28 20:00
Author: Jean-Pierre Giraud
Tags: project
Lang: vi
Translator: Trần Ngọc Quân
Status: published

Trong hai tháng qua, những người đóng góp sau đây nhận được tài khoản Phát
triển Debian:

* Ananthu C V (weepingclown)
* Andrea Pappacoda (tachi)
* Athos Coimbra Ribeiro (athos)
* Gioele Barabucci (gioele)
* Jongmin Kim (jmkim)
* Shengqi Chen (harry)
* Frans Spiesschaert (frans)

Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là
người Bảo trì Debian:

* Tianyu Chen
* Emmanuel FARHI
* наб
* Nicolas Schodet

Xin chúc mừng!
