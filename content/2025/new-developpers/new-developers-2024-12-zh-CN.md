Title: 新的 Debian 开发者和维护者 (2024年11月 至 年12月)
Slug: new-developers-2024-12
Date: 2025-01-28 20:00
Author: Jean-Pierre Giraud
Tags: project
Lang: zh-CN
Translator: Boyuan Yang
Status: published

下列贡献者在过去的两个月中获得了他们的 Debian 开发者帐号：

* Ananthu C V (weepingclown)
* Andrea Pappacoda (tachi)
* Athos Coimbra Ribeiro (athos)
* Gioele Barabucci (gioele)
* Jongmin Kim (jmkim)
* Shengqi Chen (harry)
* Frans Spiesschaert (frans)

下列贡献者在过去的两个月中被添加为 Debian 维护者：

* Tianyu Chen
* Emmanuel FARHI
* наб
* Nicolas Schodet

祝贺他们！
