Title: Nous desenvolupadors i mantenidors de Debian (novembre i desembre del 2024)
Slug: new-developers-2024-12
Date: 2025-01-28 20:00
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Translator:
Status: published

Els següents col·laboradors del projecte han esdevingut Debian Developers en
els darrers dos mesos:

* Ananthu C V (weepingclown)
* Andrea Pappacoda (tachi)
* Athos Coimbra Ribeiro (athos)
* Gioele Barabucci (gioele)
* Jongmin Kim (jmkim)
* Shengqi Chen (harry)
* Frans Spiesschaert (frans)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers en els
darrers dos mesos:

* Tianyu Chen
* Emmanuel FARHI
* наб
* Nicolas Schodet

Enhorabona a tots!
