Title: नए डेबियन डेवलपर्स और मेंटेनर्स (नवंबर और दिसंबर 2024)
Slug: new-developers-2024-12
Date: 2025-01-28 20:00
Author: Jean-Pierre Giraud
Tags: project
Lang: hi-IN
Translator: Yashraj Moghe
Status: published

पिछले दो महीनों में निम्नलिखित सहायकों को उनका डेबियन डेवलपर अकाउंट मिला:

* Ananthu C V (weepingclown)
* Andrea Pappacoda (tachi)
* Athos Coimbra Ribeiro (athos)
* Gioele Barabucci (gioele)
* Jongmin Kim (jmkim)
* Shengqi Chen (harry)
* Frans Spiesschaert (frans)

और निम्नलिखित को डेबियन मेंटेनर का स्थान दिया गया हैं:

* Tianyu Chen
* Emmanuel FARHI
* наб
* Nicolas Schodet

इन्हें बधाईयाँ!
