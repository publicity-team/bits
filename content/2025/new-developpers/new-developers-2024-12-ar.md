Title: متطوري ومشرفي دبيان الجدد (تشرين الثاني و كانون الأول 2024)
Slug: new-developers-2024-12
Date: 2025-01-28 20:00
Author: Jean-Pierre Giraud
Tags: project
Lang: ar
Translator: Maytham Alsudany
Status: published

حصلوا المساهمون التاليون على حسابات متطوري دبيان في الشهرين الماضيين:

* Ananthu C V (weepingclown)
* Andrea Pappacoda (tachi)
* Athos Coimbra Ribeiro (athos)
* Gioele Barabucci (gioele)
* Jongmin Kim (jmkim)
* Shengqi Chen (harry)
* Frans Spiesschaert (frans)

تمت إضافة المساهمون التاليون كمشرفي دبيان في الشهرين الماضيين:

* Tianyu Chen
* Emmanuel FARHI
* наб
* Nicolas Schodet

تهانينا!
