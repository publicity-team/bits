Title: New Debian Developers and Maintainers (November and December 2024)
Slug: new-developers-2024-12
Date: 2025-01-28 20:00
Author: Jean-Pierre Giraud
Tags: project
Lang: en
Status: published

The following contributors got their Debian Developer accounts in the last two
months:

* Ananthu C V (weepingclown)
* Andrea Pappacoda (tachi)
* Athos Coimbra Ribeiro (athos)
* Gioele Barabucci (gioele)
* Jongmin Kim (jmkim)
* Shengqi Chen (harry)
* Frans Spiesschaert (frans)

The following contributors were added as Debian Maintainers in the last two
months:

* Tianyu Chen
* Emmanuel FARHI
* наб
* Nicolas Schodet

Congratulations!
