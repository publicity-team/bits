Title: Nouveaux développeurs et mainteneurs de Debian (novembre et décembre 2024)
Slug: new-developers-2024-12
Date: 2025-01-28 20:00
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Translator:
Status: published

Les contributeurs suivants sont devenus développeurs Debian ces deux derniers
mois :

* Ananthu C V (weepingclown)
* Andrea Pappacoda (tachi)
* Athos Coimbra Ribeiro (athos)
* Gioele Barabucci (gioele)
* Jongmin Kim (jmkim)
* Shengqi Chen (harry)
* Frans Spiesschaert (frans)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la même
période :

* Tianyu Chen
* Emmanuel FARHI
* наб
* Nicolas Schodet

Félicitations !
