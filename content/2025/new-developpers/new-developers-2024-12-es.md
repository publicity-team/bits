Title: Nuevos desarrolladores y mantenedores de Debian (noviembre y diciembre del 2024)
Slug: new-developers-2024-12
Date: 2025-01-28 20:00
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Translator:
Status: published

Los siguientes colaboradores del proyecto se convirtieron en Debian Developers
en los dos últimos meses:

* Ananthu C V (weepingclown)
* Andrea Pappacoda (tachi)
* Athos Coimbra Ribeiro (athos)
* Gioele Barabucci (gioele)
* Jongmin Kim (jmkim)
* Shengqi Chen (harry)
* Frans Spiesschaert (frans)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers
en los dos últimos meses:

* Tianyu Chen
* Emmanuel FARHI
* наб
* Nicolas Schodet

¡Felicidades a todos!
