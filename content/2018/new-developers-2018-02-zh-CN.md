Title: 新 Debian 开发者和维护者（2018年1月至2月）
Slug: new-developers-2018-02
Date: 2018-03-04 08:30
Author: Jean-Pierre Giraud
Tags: project
Translator: Boyuan Yang
Lang: zh-CN
Status: published

下列贡献者在过去的两个月中获得了他们的 Debian 开发者帐号：

* Alexandre Mestiashvili (mestia)
* Tomasz Rybak (serpent)
* Louis-Philippe Véronneau (pollo)

下列贡献者在过去的两个月中被添加为 Debian 维护者：

* Teus Benschop
* Kyle John Robbertze
* Maarten van Gompel
* Dennis van Dok
* Innocent De Marchi
* David Rabel

祝贺他们！
