Title: Nous desenvolupadors i mantenidors de Debian (novembre i desembre del 2017)
Slug: new-developers-2017-12
Date: 2018-01-01 20:30
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Status: published

Els següents col·laboradors del projecte han esdevingut Debian Developers
en els darrers dos mesos:

* Ben Armstrong (synrg)
* Frédéric Bonnard (frediz)
* Jerome Charaoui (lavamind)
* Michael Jeanson (mjeanson)
* Jim Meyering (meyering)
* Christopher Knadle (krait)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers
en els darrers dos mesos:

* Chris West
* Mark Lee Garrett
* Pierre-Elliott Bécue
* Sebastian Humenda
* Stefan Schörghofer
* Stephen Gelman
* Georg Faerber
* Nico Schlömer

Enhorabona a tots!
