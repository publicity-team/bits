Title: Debian Perl Sprint 2015
Slug: perl-sprint-2015
Date: 2015-07-13 21:00
Author: Alex Muntada
Tags: perl, sprint, barcelona
Status: published

The [Debian Perl](https://wiki.debian.org/Teams/DebianPerlGroup) team had its
[first sprint](https://wiki.debian.org/Sprints/2015/DebianPerlSprint) in May
and it was a success: 7 members met in Barcelona the weekend from May 22nd to
May 24th to kick off the development around perl for Stretch and to work on QA
tasks across the more than 3000 packages that the team maintains.

Even though the participants enjoyed the beautiful weather and the food very
much, a good amount of work was also done:

* 53 bugs were filed or worked on, 31 uploads were accepted.
* The current practice of patch management (`quilt`) was discussed and
  possible alternatives were shown (`git-debcherry` and `git-dpm`).
* Improvements were made in the Debian Perl Tools (`dpt`) and discussed how to
  get track of upstream git history and tags.
* Team's policies, documentation and recurring tasks were reviewed and updated.
* Perl 5.22 release was prepared and `src:perl` plans for Stretch were
  discussed.
* `autopkgtest` whitelists were reviewed, new packages added, and IRC
  notificacions by KGB were discussed.
* Outstanding migrations were reviewed.
* Reproducibility issues with `POD_MAN_DATE` were commented.

The [full report](https://lists.debian.org/debian-perl/2015/07/msg00009.html)
was posted to the relevant Debian mailing lists.

The participants would like to thank the
[Computer Architecture Department](http://www.ac.upc.edu/) of the Universitat
Politècnica de Catalunya for hosting us, and all donors to the Debian project
who helped to cover a large part of our expenses.
