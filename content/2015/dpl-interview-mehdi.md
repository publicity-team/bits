Title: apt install dpl-candidate: Mehdi Dogguy
Slug: dpl-interview-mehdi
Date: 2015-03-14 20:36
Author: Zlatan Todorić
Tags: interviews, dpl, vote, meetDDs
Status: published

**0. Who are you and what is your history with Debian Project?**

I guess this part is well covered in my [platform].

[platform]: https://www.debian.org/vote/2015/platforms/mehdi

**1. What is your most proud moment as Debian Developer?**

I am pretty proud of having been part of the few who implemented
the first automatic dependency resolver for OCaml programs and
libraries in Debian packages. It was really the first one in the
OCaml community and we were quite proud of it. But that was done
_before_ I become a Debian Developer.

As a DD, I have to admit I am quite proud to be part of the Release
Team. It is a fantastic team where there is so much to do. Helping
the team means something to me, and I invested a considerable amount
of time (a few months) working on reviewing patches for Squeeze and
helping to get it ready by our standards. My best moment was Squeeze's
release, my first Debian release as Release Team member.

**2. In your opinion what is the strongest part of Debian Project?**

I am not sure we can identify one single strength of the Debian project.
But, when I think about your question, I remember something I've heard
many times: “Debian is about people”. I have to admit that I didn't
realize it myself until I heard it for the first time and I completely
share the idea! For me, all the technical side of the project comes
after the community. With time, I think we managed to build a strong
community. Many contributors became friends with time. We are seeing
many Developers having babies and bringing them to Debian events. I find
that really amazing.

**3. And what is the weakest part of Debian Project?**

Our strength is somehow also our weakness. We are humans and make
mistakes. We have feelings and some discussions get heated sometimes.
It is not easy to keep everyone calm and focused. We have seen the
damage that was caused to our core community last year with all the
flamewars. Many people lost their motivation and we have seen some of
them stepping down. We are also having troubles on-boarding new
contributors, which is a problem today because some teams are
under-staffed and could become an even bigger issue on the longer term.

**4. How do you intend to resolve the weakest part?**

An effort has already been made on this front. We can mention the
introduction of the Code of Conduct and the diversity statement, for
example. Both are important and make us a more welcoming and caring
community.

In my platform, I mentioned some ideas about recruitment and change
management. I believe that both sides will help us to get a stronger
community. Moreover, a DPL should act as a mediator to help some situation
get through. This is one of the DPL tasks that is not formally identified
and is usually under-estimated.

**5. DPL term lasts for one year - what would you challenge during
that term and what have you learned from previous DPL's?**

Personally, the main thing I have learned from past DPLs is that
communication is very important. A DPL should dedicate time to
communicate about ongoing actions and achievements. It is also important
to remind a few things even if it may sound repetitive or trivial:

- Why such action/subject is important.
- What actions have been tried/done in the past.
- What progress has been made since last time.
- What is possibly the next step.

If the communication is only about listing some actions, many people
will miss its essence and its goals. It is even more important when we
know that some actions may take years (thus, several DPL terms) to
complete.

If I am elected as DPL, I'd really like to help the project to publish
a roadmap. I think it is very important to set goals to the project to
better explain our philosophy and approach in the Free Software world.
This may also help to attract new contributors which may be interested
by one or some items. Of course, I will not work on that subject only.
I invite you to read the rest of my platform to see the other ideas.

**6. What motivates you to work in Debian and run for DPL?**

Many many things. And more importantly, many many people

As many of us, I like programming and socializing. It feels nice to
be part of such a big project and where you can do many different
things. I contribute to Debian because I find it fun and let me meet
people I will not have been able to meet elsewhere.

In my platform, I tried to identify ideas I'd like to see implemented,
or at least started. Since Debian is a do-ocracy, I thought I could
try to get them implemented by myself. I think that those ideas are
important for the Debian community and will help us moving forward.
Running for DPL is also another way of contributing to Debian and I'd
feel honored to represent Debian.
