Title: apt install dpl-candidate: Neil McGovern
Slug: dpl-interview-neilm
Date: 2015-03-14 20:34
Author: Zlatan Todorić
Tags: interviews, dpl, vote, meetDDs
Status: published

**0. Who are you and what's your history with Debian Project?**

My name's Neil, I've been involved with Debian for over 10 years now.
I've held a variety of roles, from the SPI board, writing policy and
secure testing team, to being one of the Release Managers for Squeeze
and Wheezy.

**1. What's your most proud moment as Debian Developer?**

Probably the release of Squeeze, my first as RM. It was the end of a
great effort to get the release out. I particularly remember at the end
of DebConf 10 in New York going to the local Disney store and buying
every single small squeeze plush toy they had, so I could send a thank
you gift to the rest of the release team!
Another perhaps was when I first got my Debian kilt.

**2. In your opinion what is the strongest part of Debian Project?**

I think this is our social contract. It guides us, and is what we all
agree on. This is our promise to ourselves, to the wider open source
community and to our users.

**3. And what is the weakest part of Debian Project?**

At a push, I'd say it's the variety of packages we have in the archive.
I'm not sure it's weakness, but it's certainly a challenge. It becomes
exponentially harder to ensure that everything integrates well as you
add more packages. To have made it do so this far is quite impressive.

**4. How do you intend to resolve the weakest part?**

Well, see the section in my platform on PPAs, and modernising our build
and infrastructure system  Wouldn't it be great if you could stage a
package against all of stable, testing and unstable, and see what fails
to build and where, with live build logs on all architectures?

**5. DPL term lasts for one year - what would you challenge during
that term and what have you learned from previous DPL's?**

I think my primary role as DPL for 2015 would be to get a great start of
development for Stretch. The start of a new release cycle is the exact
time to implement wide changes that are potentially disruptive. Every
couple of years we seem to relax after the release, rather than get
geared up for the next one, and then time passes, and plans slip, and
before we know it, the freeze is fast approaching. If we start planning
/now/, then we can hopefully enter the freeze with fewer RC bugs, which
should be great news for everyone!
For the second part, I've talked to a lot of the previous DPLs, and
worked with them in one role or another. The main thing I was told was
that I shouldn't try and do everything I planned on. It's hard work, and
all sorts of things pop up that derail your original plans.

**6. What motivates you to  work in Debian and run for DPL?**

The people involved. I've met and worked with some of my greatest
friends due to the project. The work, dedication and commitment of those
over the years is outstanding. Most of these people are still with us,
and unfortunately, a few are not.
Whenever I'm feeling disheartened or annoyed, usually due to a giant
flame-war then I simply remember that what we're doing is truly
remarkable, and the effort that everyone has put in over the years isn't
something that should be taken for granted.
