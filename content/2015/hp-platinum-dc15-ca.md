Title: Hewlett-Packard és Patrocinador Platí de la DebConf15
Slug: hp-platinum-debconf15
Date: 2015-03-25 15:00
Author: Laura Arjona Reina
Translator: Adrià García-Alzórriz, Innocent De Marchi
Tags: debconf15, debconf, sponsors, HP
Lang: ca
Status: published

[![HPlogo](|static|/images/logo_hp.png)](http://www.hp.com/)

Ens plau anunciar que [**HP**](http://www.hp.com/) ha confirmat el seu
suport per a la DebConf15.

"*L'equip hLinux es plau de continuar la llarga tradició d'HP de
recolzar Debian i la DebConf*," ha dit Steve Geary, Director Sènior de
Hewlett-Packard.

Hewlett-Packard és una de les empreses d'informàtica més grans del món,
oferint un ampli ventall de productes i serveis com ara servidors,
ordinadors, impressores, productes d'emmagatzematge, equipament de
xarxa, programari, solucions de computació distribuïda, etc.

Hewlett-Packard ha estat un aliat en el desenvolupament de Debian des
de fa temps, proveint maquinari, miralls i altres serveis (els
donatius de maquinari d'HP estan llistats a la pàgina de [màquines
Debian](https://db.debian.org/machines.cgi) ).

Amb aquesta confirmació com a Patrocinador Platí, HP ajuda a fer
possible la nostra conferència anual i recolza directament el progrés
de Debian i el Programari lliure, ajudant a enfortir la comunitat que
segueix col·laborant en els seus projectes a Debian la resta de l'any.

Moltes gràcies Hewlett-Packard, pel teu recolzament a la DebConf15!

## Esdevingueu també un patrocinador!

La DebConf15 encara accepta patrocinadors. Les empreses i
organitzacions interessades es poden posar en contacte amb l'equip de
la DebConf a través de [sponsors@debconf.org](mailto:sponsors@debconf.org)
o bé visitant la pàgina web de la DebConf15 a
[http://debconf15.debconf.org](http://debconf15.debconf.org).
