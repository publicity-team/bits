Title: Nouveaux développeurs et mainteneurs de Debian (mars et avril 2016)
Slug: new-developers-2016-04
Date: 2016-05-17 00:10
Author: Ana Guerrero Lopez
Tags: project
Lang: fr
Status: published

Les contributeurs suivants sont devenus développeurs Debian ces deux
derniers mois :

* Sven Bartscher (kritzefitz)
* Harlan Lieberman-Berg (hlieberman)

Félicitations !
