Title: 新的 Debian 开发者和维护者 (2019年11月 至 年12月)
Slug: new-developers-2019-12
Date: 2020-01-26 15:00
Author: Jean-Pierre Giraud
Tags: project
Lang: zh-CN
Translator: Boyuan Yang
Status: published

下列贡献者在过去的两个月中获得了他们的 Debian 开发者帐号：

* Louis-Philippe Véronneau (pollo)
* Olek Wojnar (olek)
* Sven Eckelmann (ecsv)
* Utkarsh Gupta (utkarsh)
* Robert Haist (rha)

下列贡献者在过去的两个月中被添加为 Debian 维护者：

* Denis Danilov
* Joachim Falk
* Thomas Perret
* Richard Laager

祝贺他们！
