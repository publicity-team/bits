Title: Nous desenvolupadors i mantenidors de Debian (setembre i octubre del 2020)
Slug: new-developers-2020-10
Date: 2020-11-16 20:00
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Status: published

Els següents col·laboradors del projecte han esdevingut Debian Developers en
els darrers dos mesos:

* Benda XU (orv)
* Joseph Nahmias (jello)
* Marcos Fouces (marcos)
* Hayashi Kentaro (kenhys)
* James Valleroy (jvalleroy)
* Helge Deller (deller)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers en
els darrers dos mesos:

* Ricardo Ribalda Delgado
* Pierre Gruet
* Henry-Nicolas Tourneur
* Aloïs Micard
* Jérôme Lebleu
* Nis Martensen
* Stephan Lachnit
* Felix Salfelder
* Aleksey Kravchenko
* Étienne Mollier

Enhorabona a tots!
