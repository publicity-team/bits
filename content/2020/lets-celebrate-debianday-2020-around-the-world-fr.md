Title: Célébrons la Journée Debian 2020 dans le monde entier
Date: 2020-07-22 15:30
Tags: debian, birthday
Slug: lets-celebrate-debianday-2020-around-the-world
Author: Paulo Henrique de Lima Santana (phls)
Lang: fr
Translator: Jean-Pierre Giraud
Status: published

Nous encourageons notre communauté à célébrer dans le monde entier le
vingt-septième anniversaire de Debian avec des événements organisés pour la
[Journée Debian](https://wiki.debian.org/DebianDay) ! Cette année, du fait
de la pandémie de COVID-19, nous ne pouvons pas organiser de réunions
publiques, aussi, nous invitons plutôt les contributeurs, les développeurs,
les équipes, les responsables et les utilisateurs de Debian à promouvoir en
ligne le projet Debian par des activités le 16 août (et éventuellement le 15).

Les communautés peuvent organiser un programme complet d'activités en ligne
pour toute la journée. Ce pourra être des présentations, des ateliers, des
activités interactives telles qu’une aide à des traductions ou à leur
édition, des débats, des réunions, etc., et tout cela dans leur langue, avec
des outils tels que [Jitsi](https://meet.jit.si) pour des capture audio et
vidéo des présentateurs et pour leur diffusion ultérieure sur YouTube.

Si vous ne connaissez pas de communauté locale qui organise un événement, ou
si vous ne souhaitez pas vous joindre à l'un deux, vous pouvez concevoir
tout seul votre propre activité avec [OBS](https://obsproject.com) et la
diffuser sur YouTube.
Vous trouverez un tutoriel sur OBS
[ici](https://peertube.debian.social/videos/watch/7f41c0e7-66cc-4234-b929-6b3219d95c14).

N'oubliez pas d'enregistrer votre activité parce que ce pourra être une
très bonne idée de l'envoyer plus tard sur
[Peertube](https://peertube.debian.social).

Veuillez ajouter votre événement ou activité sur la
[page wiki DebianDay](https://wiki.debian.org/DebianDay/2020)
et faites la nous connaître pour en faire la publicité sur
[Debian micronews](https://micronews.debian.org).
Pour la partager, vous avez plusieurs possibilités :

* suivre les étapes décrites
  [ici](https://micronews.debian.org/pages/contribute.html) pour les
  développeurs Debian ;
* nous contacter par IRC sur le canal `#debian-publicity` sur le réseau OFTC et
  nous le demander ;
* envoyer un courriel à
  [debian-publicity@lists.debian.org](https://lists.debian.org/debian-publicity/)
  et demander que votre action soit incluse dans les Debian micronews. C'est
  une liste archivée publiquement.

P.-S. : DebConf20 en ligne approche ! Elle se tiendra du 23 au 29 août 2020.
[Les inscriptions sont déjà ouvertes](https://debconf20.debconf.org/news/2020-07-12-registration-is-open/).
