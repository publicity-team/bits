Title: "Homeworld" será o novo tema padrão do Debian 11
Slug: homeworld-will-be-the-default-theme-for-debian-11
Date: 2020-11-12 13:30
Author: Jonathan Carter
Artist: Juliette Taka
Translator: Carlos Henrique Lima Melara, Thiago Pezzo (Tico)
Lang: pt-BR
Tags: bullseye, artwork
Status: published

O tema ["Homeworld"][homeworld-link] proposto por Juliette Taka foi selecionado
como tema padrão para o Debian 11 "bullseye". Juliette diz que esse tema
foi inspirado no movimento Bauhaus, um estilo artístico nascido na Alemanha no
século 20.

[![Papel de parede Homeworld. Clique para ver a proposta de tema completa](|static|/images/homeworld_wallpaper.png)][homeworld-link]

[![Tema Homeworld debian-installer. Clique para ver a proposta de tema completa](|static|/images/homeworld_debian-installer.png)][homeworld-link]

[homeworld-link]: https://wiki.debian.org/DebianArt/Themes/Homeworld

Após o time Debian Desktop ter feito a
[chamada para propostas de temas](https://lists.debian.org/debian-devel-announce/2020/08/msg00002.html),
um total de [dezoito escolhas](https://wiki.debian.org/DebianDesktop/Artwork/Bullseye)
foram submetidas. A escolha da ilustração da área de trabalho foi aberta ao
público e recebemos 5613 respostas ranqueando as diferentes propostas. Dentre
elas, o tema Homeworld foi o vitorioso.

Esta é a terceira vez que uma proposta feita pela Juliette ganha. Ela é
também a autora do [tema Lines](https://wiki.debian.org/DebianArt/Themes/Lines)
que foi usado no Debian 8 e do
[tema softWaves](https://wiki.debian.org/DebianArt/Themes/softWaves)
usado no Debian 9.

Nós gostaríamos de agradecer a todos(as) os(as) artistas que participaram e
submeteram seus excelentes trabalhos na forma de papéis de parede e ilustrações
para o Debian 11.

Parabéns, Juliette, e muito obrigado por sua contribuição para o Debian!
