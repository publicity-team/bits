Title: Le site web de Debian a mis à jour sa page d'accueil et prépare un profond renouvellement
Slug: debian-web-new-homepage
Date: 2020-12-17 13:50
Author: Laura Arjona Reina
Artist:
Lang: fr
Translator: Jean-Pierre Giraud
Tags: web
Status: published

Aujourd'hui, le [site web de Debian](https://www.debian.org) affiche une
nouvelle page d'accueil. Depuis la dernière
[réunion](https://bits.debian.org/2019/04/debian-web-team-sprint-2019.html) de
l'équipe web en mars 2019, nous avons travaillé à renouveler la structure, le
contenu, la présentation et les scripts qui construisent le site. L'essentiel
du travail a porté sur deux secteurs : le retrait ou la mise à jour de contenus
obsolètes et la création d'une nouvelle page d'accueil qui soit plus attrayante
pour les nouveaux venus. Cette dernière permet maintenant de mettre plus en
lumière les aspects sociaux du projet Debian, en plus du système d'exploitation
que nous développons.

[![Site web de Debian : une partie de l'ancienne page d'accueil (en arrière-plan) et la nouvelle (en premier-plan)](|static|/images/debian_web_old_new.png)](https://www.debian.org)

Bien que cela ait pris plus de temps que nous l'espérions et que nous ne
considérions pas cette nouvelle page d'accueil comme définitive, nous
pensons que c'est un excellent premier pas vers un meilleur site web.

L'équipe web va continuer son travail de restructuration du site de Debian.
Nous aimerions lancer un appel à la communauté pour qu'elle nous apporte son
aide, et nous envisageons aussi une aide extérieure dans la mesure où nous ne
sommes qu'un petit groupe dont les membres sont également impliqués dans
d'autres équipes Debian. Parmi les prochaines étapes que nous espérons franchir
figurent l'amélioration des feuilles de style, des icônes et de la présentation
en général, ainsi que la révision des contenus pour obtenir une meilleure
structure.

Si vous souhaitez apporter votre aide, veuillez nous contacter. Vous pouvez
répondre à la version de cet article (plus complète)
[publiée sur la liste de diffusion publique](https://lists.debian.org/debian-www/2020/12/msg00057.html)
(en anglais) ou discuter avec nous sur le canal IRC #debian-www (sur
irc.debian.org).
