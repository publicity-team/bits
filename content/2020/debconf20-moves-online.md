Title: DebConf20 moves online, DebConf21 will be in Haifa
Date: 2020-06-12 18:30
Tags: debconf, debconf20, debconf21
Slug: debconf20-moves-online
Author: Daniel Lange, Antonio Terceiro, Stefano Rivera
Lang: en
Status: published

The DebConf team has had to take the hard decision that DebConf 20
cannot happen in-person, in Haifa, in August, as originally planned.
This decision is based on the status of the venue in Haifa, the local
team's view of the local health situation, the existing travel
restrictions and the [results of a survey of potential participants][0].

[0]: https://salsa.debian.org/debconf-team/public/data/dc20/-/tree/master/survey

DebConf 20 will be held online instead!

The Debian community can still get together to share ideas, discuss plans in Birds of
a Feather sessions, and eat cheese, from the safety of the desks at home.

So, please [submit your talk, sprint, and BoF proposals][1] for DebConf 20 Online.

[1]: https://debconf20.debconf.org/cfp/

It will be held within the same dates, as before, 23-29 August.
The DebConf team expects the event to be significantly shorter than a usual DebCamp +
DebConf, but that will depend on the volume of proposals received.

Hopefully in 2021 we can once again hold conferences in person.
DebConf 21 is scheduled to be taking place in Haifa.
The following planned DebConfs will be held a year later
than originally scheduled: 2022 in Kosovo and 2023 in Kochi, India.

See you online in August!
