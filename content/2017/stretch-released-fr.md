Title: Debian 9.0 Stretch est publiée !
Date: 2017-06-18 08:25
Tags: stretch
Slug: stretch-released
Author: Ana Guerrero Lopez and Laura Arjona Reina
Status: published
Lang: fr
Translator: Cédric Boutillier

![Alt Stretch est publiée](|static|/images/banner_stretch.png)

Laissez-vous faire enlasser par la pieuvre en caoutchouc violet.
Nous sommes heureux d'annoncer la publication de Debian 9.0 « Stretch ».

**Vous voulez l'installer ?**
Choisissez votre [support d'installation](https://www.debian.org/distrib/)
préféré parmi disques Blu-ray, DVD ou CD, et clés USB. Puis lisez le
[manuel d'installation](https://www.debian.org/releases/stretch/installmanual).

**Vous êtes déjà un utilisateur heureux de Debian, et vous voulez juste la mettre à jour ?**
Vous pouvez facilement mettre à jour votre installation de Debian 8 Jessie,
en consultant les
[notes de publication](https://www.debian.org/releases/stretch/releasenotes).

**Vous voulez célébrer la publication ?**
Partagez la [bannière de ce blog](https://wiki.debian.org/DebianArt/Themes/softWaves?action=AttachFile&do=view&target=wikiBanner.png)
sur votre blog ou votre site web !
