Title: Nous desenvolupadors i mantenidors de Debian (gener i febrer del 2017)
Slug: new-developers-2017-02
Date: 2017-03-08 00:30
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Status: published

Els següents col·laboradors del projecte han esdevingut Debian Developers
en els darrers dos mesos:

* Ulrike Uhlig (ulrike)
* Hanno Wagner (wagner)
* Jose M Calhariz (calharis)
* Bastien Roucariès (rouca)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers
en els darrers dos mesos:

* Dara Adib
* Félix Sipma
* Kunal Mehta
* Valentin Vidic
* Adrian Alves
* William Blough
* Jan Luca Naumann
* Mohanasundaram Devarajulu
* Paulo Henrique de Lima Santana
* Vincent Prat

Enhorabona a tots!
