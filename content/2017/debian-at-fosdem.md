Title: Debian at FOSDEM 2017
Slug: debian-at-fosdem-2017
Date: 2017-01-28 13:00
Author: Joost van Baal-Ilić
Tags: debian, fosdem
Status: published

On February 4th and 5th, Debian will be attending
[FOSDEM 2017](https://fosdem.org/2017) in Brussels,
Belgium; a yearly gratis event (no registration needed) run by volunteers from
the Open Source and Free Software community.  It's free, and it's big: more than
600 speakers, over 600 events, in 29 rooms.

This year more than 45 current or past
Debian contributors will [speak](https://fosdem.org/2017/schedule/speakers/) at FOSDEM:
Alexandre Viau,
Bradley M. Kuhn,
Daniel Pocock,
Guus Sliepen,
Johan Van de Wauw,
John Sullivan,
Josh Triplett,
Julien Danjou,
Keith Packard,
Martin Pitt,
Peter Van Eynde,
Richard Hartmann,
Sebastian Dröge,
Stefano Zacchiroli
and Wouter Verhelst, among others.

<!---
see https://storm.debian.net/shared/G_8yunG6KKDVdZsH5vOWAZldrbEVs9a0tqc5uWKB7zW
to know the complete list and how we figure out the list
-->

Similar to previous years, the event will be hosted at Université libre de
Bruxelles.  Debian contributors and enthusiasts will be taking shifts at the
Debian stand with gadgets, T-Shirts and swag.  You can find us at stand number
4 in building K, 1 B; CoreOS Linux and PostgreSQL will be our neighbours. See
[https://wiki.debian.org/DebianEvents/be/2017/FOSDEM](https://wiki.debian.org/DebianEvents/be/2017/FOSDEM)
for more details.

We are looking forward to meeting you all!
