Title: Nouveaux développeurs et mainteneurs de Debian (mai et juin 2017)
Slug: new-developers-2017-06
Date: 2017-07-02 14:30
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Status: published

Les contributeurs suivants sont devenus développeurs Debian ces deux
derniers mois :

* Alex Muntada (alexm)
* Ilias Tsitsimpis (iliastsi)
* Daniel Lenharo de Souza (lenharo)
* Shih-Yuan Lee (fourdollars)
* Roger Shimizu (rosh)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la
même période :

* James Valleroy
* Ryan Tandy
* Martin Kepplinger
* Jean Baptiste Favre
* Ana Cristina Custura
* Unit 193

Félicitations !
