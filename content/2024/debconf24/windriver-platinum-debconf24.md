Title: Wind River Platinum Sponsor of DebConf24
Slug: windriver-platinum-debconf24
Date: 2024-07-16 17:08
Author: Sahil Dhiman
Artist: Wind River
Tags: debconf24, debconf, sponsors, wind river
Image: /images/windriver.png
Status: published

[![windriverlogo](|static|/images/windriver.png)](https://www.windriver.com)

We are pleased to announce that
**[Wind River](https://www.windriver.com)** has committed to sponsor
[DebConf24](https://debconf24.debconf.org/) as a **Platinum Sponsor**.

For nearly 20 years, Wind River has led in commercial open source Linux
solutions for mission-critical enterprise edge computing. With expertise
across aerospace, automotive, industrial, telecom, more, the company is
committed to open source through initiatives like eLxr, Yocto, Zephyr,
and StarlingX.

With this commitment as Platinum Sponsor, Wind River is contributing to
make possible our annual conference, and directly supporting the
progress of Debian and Free Software, helping to strengthen the
community that continues to collaborate on Debian projects throughout
the rest of the year.

Wind River plans to announce an exiting new project based on Debian at
this year's DebConf!

Thank you very much, Wind River, for your support of DebConf24!

## Become a sponsor too!

[DebConf24](https://debconf24.debconf.org/) will take place from 28th
July to 4th August 2024 in Busan, South Korea, and will be preceded by
DebCamp, from 21st to 27th July 2024.

DebConf24 is accepting sponsors! Interested companies and organizations should
contact the DebConf team through
[sponsors@debconf.org](mailto:sponsors@debconf.org), or visit the DebConf24
website at
[https://debconf24.debconf.org/sponsors/become-a-sponsor/](https://debconf24.debconf.org/sponsors/become-a-sponsor/).
