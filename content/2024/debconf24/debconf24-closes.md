Title: DebConf24 closes in Busan and DebConf25 dates announced
Date: 2024-08-10 16:00
Tags: debconf24, debconf25, announce, debconf
Slug: debconf24-closes
Author: Jean-Pierre Giraud and Donald Norwood
Artist: Aigars Mahinovs
Image: /images/debconf24_group_photo.jpg
Status: published

[![DebConf24 group photo - click to enlarge](|static|/images/debconf24_group_photo.jpg)](https://wiki.debian.org/DebConf/24/Photos?action=AttachFile&do=view&target=debconf24_group.jpg)

On Saturday 3 August 2024, the annual Debian Developers and Contributors
Conference came to a close.

Over 339 attendees representing 48 countries from around the world came
together for a combined 108 events made up of more than 50 Talks and
Discussions, 37 Birds of a Feather (BoF – informal meeting between
developers and users) sessions, 12 workshops, and activities in support
of furthering our distribution and free software (25 patches submitted
to the Linux kernel), learning from our mentors and peers, building our
community, and having a bit of fun.

The conference was preceded by the annual
[DebCamp](https://wiki.debian.org/DebCamp) hacking session held July 21st
through July 27th where Debian Developers and Contributors convened to
focus on their Individual Debian-related projects or work in team sprints
geared toward in-person collaboration in developing Debian.

This year featured a BootCamp that was held for newcomers with a GPG
Workshop and a focus on Introduction to creating .deb files (Debian
packaging) staged by a team of dedicated mentors who shared hands-on
experience in Debian and offered a deeper understanding of how to work in
and contribute to the community.

The actual Debian Developers Conference started on Sunday July 28 2024.

In addition to the traditional 'Bits from the DPL' talk, the continuous
key-signing party, lightning talks and the announcement of next year's
DebConf25, there were several update sessions shared by internal projects
and teams.

Many of the hosted discussion sessions were presented by our technical
core teams with the usual and useful meet the Technical Committee and the
ftpteam and a set of BoFs about packaging policy and Debian infrastructure,
including talk about APT and Debian Installer and an overview about the
first eleven years of Reproducible Builds. Internationalization and
localization have been subject of several talks. The Python, Perl, Ruby,
and Go programming language teams, as well as Med team, also shared updates
on their work and efforts.

More than fifteen BoFs and talks about community, diversity and local
outreach highlighted the work of various team involved in the social
aspect of our community. This year again, Debian Brazil shared strategy
and action to attract and retain new contributors and members and
opportunities both in Debian and F/OSS.

The [schedule](https://debconf24.debconf.org/schedule/)
was updated each day with planned and ad-hoc activities introduced by
attendees over the course of the conference. Several traditional activities
 took place: a job fair, a poetry performance, the traditional
Cheese and Wine party, the group photos and the Day Trips.

For those who were not able to attend, most of the talks and sessions were
broadcast live and recorded and the videos made available through a link in
their summary in the
[schedule](https://debconf24.debconf.org/schedule/).
Almost all of the sessions facilitated remote participation via IRC
messaging apps or online collaborative text documents which allowed remote
attendees to 'be in the room' to ask questions or share comments with the
speaker or assembled audience.

DebConf24 saw over 6.8 TiB (4.3 TiB in 2023) of data streamed, 91.25
hours (55 in 2023) of scheduled talks, 20 network access points, 1.6 km
fibers (1 broken fiber...) and 2.2 km UTP cable deployed, more than 20
country Geoip viewers, 354 T-shirts, 3 day trips, and up to 200 meals
planned per day.

All of these events, activities, conversations, and streams coupled with our
love, interest, and participation in Debian and F/OSS certainly made this
conference an overall success both here in Busan, South Korea and online
around the world.

The [DebConf24 website](https://debconf24.debconf.org/)
will remain active for archival purposes and will continue to offer
links to the presentations and videos of talks and events.

Next year, [DebConf25](https://wiki.debian.org/DebConf/25) will be held
in Brest, France, from Monday, July 7 to Monday, July 21, 2025. As
tradition follows before the next DebConf the local organizers in France
will start the conference activities with DebCamp with particular focus on
individual and team work towards improving the distribution.

DebConf is committed to a safe and welcome environment for all
participants. See the
[web page about the Code of Conduct in DebConf24 website](https://debconf24.debconf.org/about/coc/)
for more details on this.

Debian thanks the commitment of numerous
[sponsors](https://debconf24.debconf.org/sponsors/)
to support DebConf24, particularly our Platinum Sponsors:
[**Infomaniak**](https://www.infomaniak.com),
[**Proxmox**](https://www.proxmox.com/),
and [**Wind River**](https://www.river.com/).

We also wish to thank our Video and Infrastructure teams, the DebConf24
and DebConf committees, our host nation of South Korea, and each and every
person who helped contribute to this event and to Debian overall.

Thank you all for your work in helping Debian continue to be "The Universal
Operating System".

See you next year!

### About Debian

The Debian Project was founded in 1993 by Ian Murdock to be a truly free
community project. Since then the project has grown to be one of the
largest and most influential open source projects. Thousands of
volunteers from all over the world work together to create and maintain
Debian software. Available in 70 languages, and supporting a huge range
of computer types, Debian calls itself the _universal operating system_.

### About DebConf

DebConf is the Debian Project's developer conference. In addition to a
full schedule of technical, social and policy talks, DebConf provides an
opportunity for developers, contributors and other interested people to
meet in person and work together more closely. It has taken place
annually since 2000 in locations as varied as Scotland, Argentina, Bosnia
and Herzegovina, and India. More information about DebConf is available from
[https://debconf.org/](https://debconf.org).

### About Infomaniak

[**Infomaniak**](https://www.infomaniak.com) is an independent cloud service
provider recognized throughout Europe for its commitment to privacy, the
local economy and the environment. Recording growth of 18% in 2023, the
company is developing a suite of online collaborative tools and cloud
hosting, streaming, marketing and events solutions. Infomaniak uses
exclusively renewable energy, builds its own data centers and develops its
solutions in Switzerland, without relocating. The company powers the
website of the Belgian radio and TV service (RTBF) and provides streaming
for more than 3,000 TV and radio stations in Europe.

### About Proxmox

[**Proxmox**](https://www.proxmox.com/) provides powerful and user-friendly
Open Source server software. Enterprises of all sizes and industries use
Proxmox solutions to deploy efficient and simplified IT infrastructures,
minimize total cost of ownership, and avoid vendor lock-in. Proxmox also
offers commercial support, training services, and an extensive partner
ecosystem to ensure business continuity for its customers. Proxmox Server
Solutions GmbH was established in 2005 and is headquartered in Vienna,
Austria. Proxmox builds its product offerings on top of the Debian
operating system.

### About Wind River

[**Wind River**](https://www.windriver.com/)  For nearly 20 years, Wind River
has led in commercial Open Source Linux solutions for mission-critical
enterprise edge computing. With expertise across aerospace, automotive,
industrial, telecom, and more, the company is committed to Open Source
through initiatives like eLxr, Yocto, Zephyr, and StarlingX.

### Contact Information

For further information, please visit the DebConf24 web page at
[https://debconf24.debconf.org/](https://debconf24.debconf.org/) or send
mail to <press@debian.org>.
