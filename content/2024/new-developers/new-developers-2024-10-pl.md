Title: Nowi Deweloperzy i Opiekunowie Debiana (wrzesień i październik 2024)
Slug: new-developers-2024-10
Date: 2024-11-28 18:00
Author: Jean-Pierre Giraud
Tags: project
Lang: pl
Translator: Grzegorz Szymaszek
Status: published

Następujący współtwórcy otrzymali konta Deweloperów Debiana w&nbsp;ciągu
ostatnich dwóch miesięcy:

* Joachim Bauch (fancycode)
* Alexander Kjäll (capitol)
* Jan Mojžíš (janmojzis)
* Xiao Sheng Wen (atzlinux)

Następujący współtwórcy dołączyli do Opiekunów Debiana w&nbsp;ciągu ostatnich
dwóch miesięcy:

* Alberto Bertogli
* Alexis Murzeau
* David Heilderberg
* Xiyue Deng
* Kathara Sasikumar
* Philippe Swartvagher

Gratulacje!
