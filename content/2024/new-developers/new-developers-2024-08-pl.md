Title: Nowi Deweloperzy i Opiekunowie Debiana (lipiec i sierpień 2024)
Slug: new-developers-2024-08
Date: 2024-09-30 16:30
Author: Jean-Pierre Giraud
Tags: project
Lang: pl
Translator: Grzegorz Szymaszek
Status: published

Następujący współtwórcy otrzymali konta Deweloperów Debiana w&nbsp;ciągu
ostatnich dwóch miesięcy:

* Carlos Henrique Lima Melara (charles),
* Joenio Marques da Costa (joenio),
* Blair Noctis (ncts).

Następujący współtwórcy dołączyli do Opiekunów Debiana w&nbsp;ciągu ostatnich
dwóch miesięcy:

* Taihsiang Ho.

Gratulacje!
