Title: Nouveaux développeurs et mainteneurs de Debian (mai et juin 2024)
Slug: new-developers-2024-06
Date: 2024-07-19 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Translator:
Status: published

Les contributeurs suivants sont devenus développeurs Debian ces deux derniers
mois :

* Dennis van Dok (dvandok)
* Peter Wienemann (wiene)
* Quentin Lejard (valde)
* Sven Geuer (sge)
* Taavi Väänänen (taavi)
* Hilmar Preusse (hille42)
* Matthias Geiger (werdahias)
* Yogeswaran Umasankar (yogu)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la même
période :

* Bernhard Miklautz
* Felix Moessbauer
* Maytham Alsudany
* Aquila Macedo
* David Lamparter
* Tim Theisen
* Stefano Brivio
* Shengqi Chen

Félicitations !
