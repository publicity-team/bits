Title: Nuevos desarrolladores y mantenedores de Debian (julio y agosto del
       2024)
Slug: new-developers-2024-08
Date: 2024-09-25 16:30
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Translator:
Status: published

Los siguientes colaboradores del proyecto se convirtieron en Debian Developers
en los dos últimos meses:

* Carlos Henrique Lima Melara (charles)
* Joenio Marques da Costa (joenio)
* Blair Noctis (ncts)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers
en los dos últimos meses:

* Taihsiang Ho

¡Felicidades a todos!
