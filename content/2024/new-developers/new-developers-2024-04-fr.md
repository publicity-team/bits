Title: Nouveaux développeurs et mainteneurs de Debian (mars et avril 2024)
Slug: new-developers-2024-04
Date: 2024-05-31 18:00
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Translator:
Status: published

Les contributeurs suivants sont devenus développeurs Debian ces deux derniers
mois :

* Patrick Winnertz (winnie)
* Fabian Gruenbichler (fabiang)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la même
période :

* Juri Grabowski
* Tobias Heider
* Jean Charles Delépine
* Guilherme Puida Moreira
* Antoine Le Gonidec
* Arthur Barbosa Diniz

Félicitations !
