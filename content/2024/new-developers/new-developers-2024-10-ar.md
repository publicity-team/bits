Title: متطوري ومشرفي دبيان الجدد (اَيْلُول و تشرين الأول 2024)
Slug: new-developers-2024-10
Date: 2024-11-28 18:00
Author: Jean-Pierre Giraud
Tags: project
Lang: ar
Translator: Maytham Alsudany
Status: published

حصلوا المساهمون التاليون على حسابات متطوري دبيان في الشهرين الماضيين:

* Joachim Bauch (fancycode)
* Alexander Kjäll (capitol)
* Jan Mojžíš (janmojzis)
* Xiao Sheng Wen (atzlinux)

تمت إضافة المساهمون التاليون كمشرفي دبيان في الشهرين الماضيين:

* Alberto Bertogli
* Alexis Murzeau
* David Heilderberg
* Xiyue Deng
* Kathara Sasikumar
* Philippe Swartvagher

تهانينا!
