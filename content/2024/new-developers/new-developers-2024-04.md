Title: New Debian Developers and Maintainers (March and April 2024)
Slug: new-developers-2024-04
Date: 2024-05-31 18:00
Author: Jean-Pierre Giraud
Tags: project
Lang: en
Status: published

The following contributors got their Debian Developer accounts in the last two
months:

* Patrick Winnertz (winnie)
* Fabian Gruenbichler (fabiang)

The following contributors were added as Debian Maintainers in the last two
months:

* Juri Grabowski
* Tobias Heider
* Jean Charles Delépine
* Guilherme Puida Moreira
* Antoine Le Gonidec
* Arthur Barbosa Diniz

Congratulations!
