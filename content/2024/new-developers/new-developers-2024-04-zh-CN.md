Title: 新的 Debian 开发者和维护者 (2024年3月 至 4月)
Slug: new-developers-2024-04
Date: 2024-05-31 18:00
Author: Jean-Pierre Giraud
Tags: project
Lang: zh-CN
Translator: Boyuan Yang
Status: published

下列贡献者在过去的两个月中获得了他们的 Debian 开发者帐号：

* Patrick Winnertz (winnie)
* Fabian Gruenbichler (fabiang)

下列贡献者在过去的两个月中被添加为 Debian 维护者：

* Juri Grabowski
* Tobias Heider
* Jean Charles Delépine
* Guilherme Puida Moreira
* Antoine Le Gonidec
* Arthur Barbosa Diniz

祝贺他们！
