Title: Nowi Deweloperzy i Opiekunowie Debiana (maj i czerwiec 2024)
Slug: new-developers-2024-06
Date: 2024-07-19 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: pl
Translator: Grzegorz Szymaszek
Status: published

Następujący współtwórcy otrzymali konta Deweloperów Debiana w&nbsp;ciągu
ostatnich dwóch miesięcy:

* Dennis van Dok (dvandok)
* Peter Wienemann (wiene)
* Quentin Lejard (valde)
* Sven Geuer (sge)
* Taavi Väänänen (taavi)
* Hilmar Preusse (hille42)
* Matthias Geiger (werdahias)
* Yogeswaran Umasankar (yogu)

Następujący współtwórcy dołączyli do Opiekunów Debiana w&nbsp;ciągu ostatnich
dwóch miesięcy:

* Bernhard Miklautz
* Felix Moessbauer
* Maytham Alsudany
* Aquila Macedo
* David Lamparter
* Tim Theisen
* Stefano Brivio
* Shengqi Chen

Gratulacje!
