Title: Novos(as) desenvolvedores(as) e mantenedores(as) Debian (julho e agosto
       de 2024)
Slug: new-developers-2024-08
Date: 2024-09-25 16:30
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Translator:
Status: published

Os(As) seguintes colaboradores(as) do projeto se tornaram Desenvolvedores(as)
Debian nos últimos dois meses:

* Carlos Henrique Lima Melara (charles)
* Joenio Marques da Costa (joenio)
* Blair Noctis (ncts)

Os(As) seguintes colaboradores(as) do projeto se tornaram Mantenedores(as)
Debian nos últimos dois meses:

* Taihsiang Ho

Parabéns a todos!
