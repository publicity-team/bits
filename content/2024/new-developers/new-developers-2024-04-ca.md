Title: Nous desenvolupadors i mantenidors de Debian (març i abril del 2024)
Slug: new-developers-2024-04
Date: 2024-05-31 18:00
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Translator:
Status: published

Els següents col·laboradors del projecte han esdevingut Debian Developers en
els darrers dos mesos:

* Patrick Winnertz (winnie)
* Fabian Gruenbichler (fabiang)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers en
els darrers dos mesos:

* Juri Grabowski
* Tobias Heider
* Jean Charles Delépine
* Guilherme Puida Moreira
* Antoine Le Gonidec
* Arthur Barbosa Diniz

Enhorabona a tots!
