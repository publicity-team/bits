Title: Nuevos desarrolladores y mantenedores de Debian (mayo y junio del 2024)
Slug: new-developers-2024-06
Date: 2024-07-19 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Translator:
Status: published

Los siguientes colaboradores del proyecto se convirtieron en Debian Developers
en los dos últimos meses:

* Dennis van Dok (dvandok)
* Peter Wienemann (wiene)
* Quentin Lejard (valde)
* Sven Geuer (sge)
* Taavi Väänänen (taavi)
* Hilmar Preusse (hille42)
* Matthias Geiger (werdahias)
* Yogeswaran Umasankar (yogu)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers
en los dos últimos meses:

* Bernhard Miklautz
* Felix Moessbauer
* Maytham Alsudany
* Aquila Macedo
* David Lamparter
* Tim Theisen
* Stefano Brivio
* Shengqi Chen

¡Felicidades a todos!
