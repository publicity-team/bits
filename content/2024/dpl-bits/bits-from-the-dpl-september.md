Title: Bits from the DPL
Date: 2024-09-02
Tags: debian, project, anniversary, DebianDay, dpl, packages removing, DEP, salsa, history, Tiny tasks, help
Slug: bits-from-the-dpl-September
Author: Andreas Tille
Status: published

Dear Debian community,

this are my bits from DPL for August.

Happy Birthday Debian
---------------------

On 16th of August Debian celebrated its 31th birthday.  Since I'm
unable to write a better text than our great publicity team I'm
simply linking to their article for those who might have missed it:

  <https://bits.debian.org/2024/08/debian-turns-31.html>

Removing more packages from unstable
------------------------------------

Helmut Grohne argued for more aggressive package removal and
[sought consensus][ru1] on a way forward. He provided six examples of processes
where packages that are candidates for removal are consuming valuable
person-power. I’d like to add that the Bug of the Day initiative (see
below) also frequently encounters long-unmaintained packages with popcon
votes sometimes as low as zero, and often fewer than ten.

Helmut's email included a list of packages that would meet the suggested
removal criteria. There was some discussion about whether a popcon vote
should be included in these criteria, with arguments both [for][ru2] and
[against][ru3] it. Although I support including popcon, I acknowledge that
Helmut has a valid point in suggesting it be left out.

While I’ve read several emails in agreement, [Scott Kitterman][ru4] made
a valid point "I don't think we need more process. We just need
someone to do the work of finding the packages and filing the bugs." I
agree that this is crucial to ensure an automated process doesn’t lead
to unwanted removals. However, I don’t see "someone" stepping up to file
RM bugs against other maintainers' packages. As long as we have strict
ownership of packages, many people are hesitant to touch a package, even
for fixing it. Asking for its removal might be even less well-received.
Therefore, if an automated procedure were to create RM bugs based on
defined criteria, it could help reduce some of the social pressure.

In this aspect the opinion of [Niels Thykier][ru5] is interesting: "As
much as I want automation, I do not mind the prototype starting as a
semi-automatic process if that is what it takes to get started."

The urgency of the problem to remove packages was put by [CharlesPlessy][ru6]
into the words: "So as of today, it is much less work to
keep a package rotting than removing it."  My observation when trying to
fix the Bug of the Day exactly fits this statement.

I would love for this discussion to lead to more aggressive removals
that we can agree upon, whether they are automated, semi-automated, or
managed by a person processing an automatically generated list
(supported by an objective procedure). To use an analogy: I’ve found
that every image collection improves with aggressive pruning. Similarly,
I’m convinced that Debian will improve if we remove packages that no
longer serve our users well.

[ru1]: https://lists.debian.org/debian-devel/2024/08/msg00298.html
[ru2]: https://lists.debian.org/debian-devel/2024/08/msg00362.html
[ru3]: https://lists.debian.org/debian-devel/2024/08/msg00354.html
[ru4]: https://lists.debian.org/debian-devel/2024/08/msg00314.html
[ru5]: https://lists.debian.org/debian-devel/2024/08/msg00306.html
[ru6]: https://lists.debian.org/debian-devel/2024/08/msg00320.html

DEP14 / DEP18
-------------

There are two DEPs that affect our workflow for maintaining
packages—particularly for those who agree on using Git for Debian
packages. DEP-14 recommends a standardized layout for Git packaging
repositories, which benefits maintainers working across teams and makes
it easier for newcomers to [learn a consistent repository structure][de1].

DEP-14 stalled for various reasons. [Sam Hartman][de2] suspected it might
be because 'it doesn't bring sufficient value.' However, the assumption
that git-buildpackage is incompatible with DEP-14 is incorrect, as
confirmed by its author, [Guido Günther][de3]. As one of the two key tools
for Debian Git repositories (besides dgit) fully supports DEP-14, though
the migration from the previous default is somewhat complex.

Some investigation into mass-converting older formats to DEP-14 was
conducted by the Perl team, as [Gregor Hermann][de4] pointed out..

The discussion about DEP-14 resurfaced with the suggestion of DEP-18.
Guido Günther proposed the title [Encourage Continuous Integration and Merge
Request-Based Collaboration for Debian Packages’][de5],
which more accurately reflects the DEP's technical intent.

Otto Kekäläinen, who initiated DEP-18 (thank you, Otto), provided a good
summary of the [current status][de6]. He also assembled a very helpful
overview of Git and [GitLab usage in other Linux distros][de7].

[de1]: https://dep-team.pages.debian.net/deps/dep14/
[de2]: https://lists.debian.org/debian-devel/2024/08/msg00229.html
[de3]: https://lists.debian.org/debian-devel/2024/08/msg00212.html
[de4]: https://lists.debian.org/debian-devel/2024/08/msg00232.html
[de5]: https://salsa.debian.org/dep-team/deps/-/merge_requests/8#note_520426
[de6]: https://lists.debian.org/debian-devel/2024/08/msg00433.html
[de7]: https://lists.debian.org/debian-devel/2024/08/msg00419.html

More Salsa CI
-------------

As a result of the DEP-18 discussion, Otto Kekäläinen suggested
implementing Salsa CI for our [top popcon packages][ci1].

I believe it would be a good idea to enable CI by default across Salsa
whenever a new repository is [created][ci2].

[ci1]: https://lists.debian.org/debian-devel/2024/08/msg00318.html
[ci2]: https://lists.debian.org/debian-devel/2024/08/msg00370.html

Progress in Salsa migration
---------------------------

In my campaign, [I stated][sm1] that I aim to reduce the number of
packages maintained outside Salsa to below 2,000. As of March 28, 2024,
the count was 2,368. Today, it stands at 2,187 (UDD query: `SELECT DISTINCT
count(*) FROM sources WHERE release = 'sid' and vcs_url not like '%salsa%' ;`).

After a third of my DPL term (OMG), we've made significant progress,
reducing the amount in question (369 packages) by nearly half. I'm
pleased with the support from the DDs who moved their packages to Salsa.
Some packages were transferred as part of the Bug of the Day initiative
(see below).

[sm1]: https://lists.debian.org/debian-vote/2024/03/msg00057.html

Bug of the Day
--------------

As announced in my 'Bits from the DPL' [talk at DebConf][bd1], I started
an initiative called [Bug of the Day][bd2]. The goal is to train newcomers
in bug triaging by enabling them to tackle small, self-contained QA
tasks. We have consistently identified target packages and resolved at
least one bug per day, often addressing multiple bugs in a single
package.

In several cases, we followed the Package Salvaging procedure outlined
in the [Developers Reference][bd3]. Most instances were either welcomed by
the maintainer or did not elicit a response. Unfortunately, there was
one exception where the recipient of the Package Salvage bug expressed
significant dissatisfaction. The takeaway is to balance formal
procedures with consideration for the recipient’s perspective.

I'm pleased to confirm that the [Matrix channel][bd4] has seen an increase
in active contributors. This aligns with my hope that our efforts would
attract individuals interested in QA work. I’m particularly pleased
that, within just one month, we have had help with both fixing bugs and
improving the code that aids in bug selection.

As I aim to introduce newcomers to various teams within Debian, I also
take the opportunity to learn about each team's specific policies
myself. I rely on team members' assistance to adapt to these policies. I
find that gaining this practical insight into team dynamics is an
effective way to understand the different teams within Debian as DPL.

Another finding from this initiative, which aligns with my goal as DPL,
is that many of the packages we addressed are already on Salsa but have
not been uploaded, meaning their VCS fields are not published. This
suggests that maintainers are generally open to managing their packages
on Salsa. For packages that were not yet on Salsa, the move was
generally welcomed.

[bd1]: https://debconf24.debconf.org/talks/20-bits-from-the-dpl/
[bd2]: https://salsa.debian.org/tille/tiny_qa_tools/-/wikis/Tiny-QA-tasks
[bd3]: https://www.debian.org/doc/manuals/developers-reference/pkgs.en.html#package-salvaging
[bd4]: https://matrix.to/#/#debian-tiny-tasks:matrix.org

Publicity team wants you
------------------------

The publicity team has decided to resume regular meetings to coordinate
their efforts. Given my high regard for their work, I plan to attend
their meetings as frequently as possible, which I began doing with the
first IRC meeting.

During discussions with some team members, I learned that the team could
use additional help. If anyone interested in supporting Debian with
non-packaging tasks reads this, please consider introducing yourself to
debian-publicity@lists.debian.org. Note that this is a publicly archived
mailing list, so it's not the best place for sharing private
information.

Kind regards
   Andreas.
