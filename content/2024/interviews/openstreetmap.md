Title: OpenStreetMap migrates to Debian 12
Slug: openstreetmap-on-debian
Date: 2024-11-27 14:01
Author: Donald Norwood, Stefano Rivera, Justin B Rye
Tags: openstreetmap, debian, interviews, technical, development, ruby, bookworm
Lang: en
Status: published

You may have seen [this toot](https://en.osm.town/@osm_tech/113050911886181861)
announcing OpenStreetMap's migration to Debian on their infrastructure.

> 🚀 After 18 years on Ubuntu, we've upgraded the
> [@openstreetmap](https://en.osm.town/@openstreetmap) servers to Debian 12
> (Bookworm). 🌍 [openstreetmap.org](https://openstreetmap.org/) is now faster
> using Ruby 3.1. Onward to new mapping adventures! Thank you to the team for
> the smooth transition.
> [#OpenStreetMap](https://en.osm.town/tags/OpenStreetMap)
> [#Debian](https://en.osm.town/tags/Debian) 🤓

We spoke with Grant Slater, the Senior Site Reliability Engineer for the
OpenStreetMap Foundation. Grant shares:

**Why did you choose Debian?**

> There is a large overlap between OpenStreetMap mappers and the Debian
> community. Debian also has excellent coverage of OpenStreetMap tools and
> utilities, which helped with the decision to switch to Debian.
>
> The Debian package maintainers do an excellent job of maintaining their
> packages - e.g.: [osm2pgsql](https://packages.debian.org/bookworm/osm2pgsql),
> [osmium-tool](https://packages.debian.org/bookworm/osmium-tool) etc.
>
> Part of our reason to move to Debian was to get closer to the maintainers of
> the packages that we depend on. Debian maintainers appear to be heavily
> invested in the software packages that they support and we see critical bugs
> get fixed.

**What drove this decision to migrate?**

> OpenStreetMap.org is primarily run on actual physical hardware that our team
> manages. We attempt to squeeze as much performance from our systems as
> possible, with some services being particularly I/O bound. We ran into some
> severe I/O performance issues with kernels ~6.0 to < ~6.6 on systems with NVMe
> storage. This pushed us onto newer mainline kernels, which led us toward
> Debian. On Debian 12 we could simply install the backport kernel and the
> performance issues were solved.

**How was the transition managed?**

> Thankfully we manage our server setup nearly completely
> [with code](https://github.com/openstreetmap/chef). We also use
> [Test Kitchen](https://kitchen.ci/) with
> [inspec](https://docs.chef.io/inspec/) to test this infrastructure code. Tests
> run locally using Podman or Docker containers, but also run as part of our git
> code pipeline.
>
> We added Debian as a test target platform and fixed up the infrastructure code
> until all the tests passed. The changes required were relatively small, simple
> package name or config filename changes mostly.

**What was your timeline of transition?**

> In August 2024 we moved the www\.openstreetmap\.org Ruby on Rails servers
> across to Debian. We haven't yet finished moving everything across to Debian,
> but we will upgrade the rest when it makes sense. Some systems may wait until
> the next hardware upgrade cycle.
>
> Our focus is to build a stable and reliable platform for OpenStreetMap
> mappers.

**How has the transition from another Linux distribution to Debian gone?**

> We are still in the process of fully migrating between Linux distributions,
> but we can share that we recently moved our frontend servers to Debian 12
> (from Ubuntu 22.04) which bumped the Ruby version from 3.0 to 3.1 which
> allowed us to also upgrade the version of Ruby on Rails that we use for
> www\.openstreetmap\.org.
>
> We also changed our chef code for managing the network interfaces from using
> netplan (default in Ubuntu, made by Canonical) to directly using
> systemd-networkd to manage the network interfaces, to allow commonality
> between how we manage the interfaces in Ubuntu and our upcoming Debian
> systems. Over the years we've standardised our networking setup to use 802.3ad
> bonded interfaces for redundancy, with VLANs to segment traffic; this setup
> worked well with systemd-networkd.
>
> We use [netboot.xyz](https://netboot.xyz/) for PXE networking booting OS
> installers for our systems and use IPMI for the out-of-band management. We
> remotely re-installed a test server to Debian 12, and fixed a few minor issues
> missed by our chef tests. We were pleasantly surprised how smoothly the
> migration to Debian went.
>
> In a few limited cases we've used Debian Backports for a few packages where
> we've absolutely had to have a newer feature. The Debian package maintainers
> are fantastic.
>
> What definitely helped us is our code is libre/free/open-source, with most of
> the core OpenStreetMap software like osm2pgsql already in Debian and well
> packaged.
>
> In some cases we do run pre-release or custom patches of OpenStreetMap
> software; with Ubuntu we used launchpad.net's Personal Package Archives (PPA)
> to build and host deb repositories for these custom packages. We were
> initially perplexed by the myriad of options in Debian (see
> [this list](https://wiki.debian.org/DebianRepository/Setup) - _eeek!_), but
> received some helpful guidance from a Debian contributor and we now manage our
> own deb repository using aptly. For the moment we're currently building deb
> packages locally and pushing to aptly; ideally we'd like to replace this with
> a git driven pipeline for building the custom packages in the future.

**Thank you for taking the time to share your experience with us.**

> Thank you to all the awesome people who make Debian!

***

We are overjoyed to share this in-use case which demonstrates our commitment to
stability, development, and long term support. Debian offers users, companies,
and organisations the ability to plan, scope, develop, and maintain at their own
pace using a rock solid stable Linux distribution with responsive developers.

Does your organisation use Debian in some capacity? We would love to hear about
it and your use of 'The Universal Operating System'. Reach out to us at
[Press@debian.org](mailto:press@debian.org) - we would be happy to add your
organisation to our ['Who's Using Debian?'](https://www.debian.org/users/) page
and to share your story!

## About Debian

The Debian Project is an association of individuals who have made common cause
to create a free operating system. This operating system that we have created is
called [Debian](https://www.debian.org/). Installers and images, such as live
systems, offline installers for systems without a network connection, installers
for other CPU architectures, or cloud instances, can be found at
[Getting Debian](https://www.debian.org/distrib/).
