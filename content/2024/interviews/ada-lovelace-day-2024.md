Title: Ada Lovelace Day 2024 - Interview with some Women in Debian
Slug: ada-lovelace-day-2024-interview-with-some-women-in-debian
Date: 2024-10-21 00:01
Author: Anupa Ann Joseph
Tags: debian, interviews, women, ada lovelace day, debian women, diversity, outreach
Lang: en
Status: published

![Alt Ada Lovelace portrait](|static|/images/ada_lovelace.png)

*[Ada Lovelace Day](https://en.wikipedia.org/wiki/Ada_Lovelace_Day) was
celebrated on October 8 in 2024, and on this occasion, to celebrate and
raise awareness of the contributions of women to the STEM fields we
interviewed some of the women in Debian.*

*Here we share their thoughts, comments, and concerns with the hope of inspiring
more women to become part of the Sciences, and of course, to work inside of
Debian.*

This article was simulcasted to the [debian-women](https://lists.debian.org/debian-women/2024/10/msg00000.html) mail list.

### *Beatrice Torracca*

**1. Who are you?**

I am Beatrice, I am Italian. Internet technology and everything computer-related
is just a hobby for me, not my line of work or the subject of my academic
studies. I have too many interests and too little time. I would like to do lots
of things and at the same time I am too Oblomovian to do any.

**2. How did you get introduced to Debian?**

As a user I started using newsgroups when I had my first dialup connection and
there was always talk about this strange thing called
[Linux](https://www.linux.org/). Since moving from DR DOS to Windows was a shock
for me, feeling like I lost the control of my machine, I tried Linux with
[Debian Potato](https://www.debian.org/releases/potato/) and I never strayed
away from Debian since then for my personal equipment.

**3. How long have you been into Debian?**

Define "into". As a user... since Potato, too many years to count. As a
contributor, a similar amount of time, since early 2000 I think. My first
archived email about contributing to the translation of the description of
Debian packages dates 2001.

**4. Are you using Debian in your daily life? If yes, how?**

Yes!! I use testing. I have it on my desktop PC at home and I have it on my
laptop. The desktop is where I have a local IMAP server that fetches all the
mails of my email accounts, and where I sync and back up all my data. On both I
do day-to-day stuff (from email to online banking, from shopping to taxes), all
forms of entertainment, a bit of work if I have to work from home
([GNU R](https://www.r-project.org/) for statistics,
[LibreOffice](https://www.libreoffice.org/)... the usual suspects). At work I am
required to have another OS, sadly, but I am working on setting up a
[Debian Live](https://www.debian.org/devel/debian-live) system to use there too.
Plus if at work we start doing bioinformatics there might be a Linux machine in
our future... I will of course suggest and hope for a Debian system.

**5. Do you have any suggestions to improve women's participation in Debian?**

This is a tough one. I am not sure. Maybe, more visibility for the women already
in the Debian Project, and make the newcomers feel seen, valued and welcomed. A
respectful and safe environment is key too, of course, but I think Debian made
huge progress in that aspect with the
[Code of Conduct](https://www.debian.org/code_of_conduct). I am a big fan of
promoting diversity and inclusion; there is always room for improvement.

### *Ileana Dumitrescu (ildumi)*

**1. Who are you?**

I am just a girl in the world who likes cats and packaging
[Free Software](https://www.gnu.org/philosophy/free-sw.html).

**2. How did you get introduced to Debian?**

I was tinkering with a computer running Debian a few years ago, and I decided to
learn more about Free Software. After a search or two, I found
[Debian Women](https://www.debian.org/women/).

**3. How long have you been into Debian?**

I started looking into contributing to Debian in 2021. After contacting Debian
Women, I received a lot of information and helpful advice on different ways I
could contribute, and I decided package maintenance was the best fit for me. I
eventually became a Debian Maintainer in 2023, and I continue to maintain a few
packages in my spare time.

**4. Are you using Debian in your daily life? If yes, how?**

Yes, it is my favourite GNU/Linux operating system! I use it for email,
chatting, browsing, packaging, etc.

**5. Do you have any suggestions to improve women's participation in Debian?**

The [mailing list for Debian Women](https://lists.debian.org/debian-women) may
attract more participation if it is utilized more. It is where I started, and I
imagine participation would increase if it is more engaging.

### *Kathara Sasikumar (kathara)*

**1. Who are you?**

I'm Kathara Sasikumar, 22 years old and a recent Debian user turned Maintainer
from India. I try to become a creative person through sketching or playing
guitar chords, but it doesn't work! xD

**2. How did you get introduced to Debian?**

When I first started college, I was that overly enthusiastic student who signed
up for every club and volunteered for anything that crossed my path just like
every other fresher.

But then, the pandemic hit, and like many, I hit a low point. COVID depression
was real, and I was feeling pretty down. Around this time, the
[FOSS Club](https://fossnss.org) at my college suddenly became more active.
My friends, knowing I had a love for free software, pushed me to join the club.
They thought it might help me lift my spirits and get out of the slump I was in.

At first, I joined only out of peer pressure, but once I got involved, the club
really took off. FOSS Club became more and more active during the pandemic, and
I found myself spending more and more time with it.

A year later, we had the opportunity to host a
[MiniDebConf](https://in2022.mini.debconf.org/) at our college. Where I got to
meet a lot of Debian developers and maintainers, attending their talks
and talking with them gave me a wider perspective on Debian, and I loved the
Debian philosophy.

At that time, I had been distro hopping but never quite settled down. I
occasionally used Debian but never stuck around. However, after the MiniDebConf,
I found myself using Debian more consistently, and it truly connected with me.
The community was incredibly warm and welcoming, which made all the difference.

**3. How long have you been into Debian?**

Now, I've been using Debian as my daily driver for about a year.

**4. Are you using Debian in your daily life? If yes, how?**

It has become my primary distro, and I use it every day for continuous learning
and working on various software projects with free and open-source tools. Plus,
I've recently become a Debian Maintainer (DM) and have taken on the
responsibility of maintaining a few packages. I'm looking forward to
contributing more to the Debian community 🙂

### *Rhonda D'Vine (rhonda)*

**1. Who are you?**

My name is Rhonda, my pronouns are she/her, or per/pers. I'm 51 years old,
working in IT.

**2. How did you get introduced to Debian?**

I was already looking into Linux because of university, first it was
[SuSE](https://www.suse.com). And people played around with gtk. But when they
packaged [GNOME](https://www.gnome.org/) and it just didn't even install I
looked for alternatives. A working colleague from back then gave me a CD of
Debian.  Though I couldn't install from it because
[Slink](https://www.debian.org/releases/slink/) didn't recognize the pcmcia
drive.  I had to install it via floppy disks, but apart from that it was
quite well done. And the early GNOME was working, so I never looked back. 🙂

**3. How long have you been into Debian?**

Even before I was more involved, a colleague asked me whether I could help with
translating the release documentation. That was my first contribution to Debian,
for the slink release in early 1999.  And I was using some other software before
on my SuSE systems, and I wanted to continue to use them on Debian obviously. So
that's how I got involved with packaging in Debian. But I continued to help with
translation work, for a long period of time I was almost the only person active
for the German part of the website.

**4. Are you using Debian in your daily life? If yes, how?**

Being involved with Debian was a big part of the reason I got into my jobs since
a long time now. I always worked with maintaining Debian (or
[Ubuntu](https://ubuntu.com/)) systems.
Privately I run Debian on my laptop, with occasionally switching to Windows in
dual boot when (rarely) needed.

**5. Do you have any suggestions to improve women's participation in Debian?**

There are factors that we can't influence, like that a lot of women are pushed
into care work because patriarchal structures work that way, and don't have the
time nor energy to invest a lot into other things.  But we could learn to
appreciate smaller contributions better, and not focus so much on the quantity
of contributions. When we look at longer discussions on mailing lists, those
that write more mails actually don't contribute more to the discussion, they
often repeat themselves without adding more substance. Through working on our
own discussion patterns this could create a more welcoming environment for a lot
of people.

### *Sophie Brun (sophieb)*

**1. Who are you?**

I'm a 44 years old French woman. I'm married and I have 2 sons.

**2. How did you get introduced to Debian?**

In 2004 my boyfriend (now my husband) installed Debian on my personal computer
to introduce me to Debian. I knew almost nothing about Open Source. During my
engineering studies, a professor mentioned the existence of Linux,
[Red Hat](https://www.redhat.com) in particular, but without giving any details.

I learnt Debian by using and reading (in advance)
[The Debian Administrator's Handbook](https://debian-handbook.info).

**3. How long have you been into Debian?**

I've been a user since 2004. But I only started contributing to Debian in 2015:
I had quit my job and I wanted to work on something more meaningful. That's why
I joined my husband in [Freexian](https://www.freexian.com/), his company.
Unlike most people I think, I started contributing to Debian for my work. I only
became a DD in 2021 under gentle social pressure and when I felt confident
enough.

**4. Are you using Debian in your daily life? If yes, how?**

Of course I use Debian in my professional life for almost all the tasks: from
administrative tasks to Debian packaging.

I also use Debian in my personal life. I have very basic needs:
[Firefox](https://www.mozilla.org/firefox),
[LibreOffice](https://www.libreoffice.org/), [GnuCash](https://www.gnucash.org/)
and [Rhythmbox](https://wiki.gnome.org/Apps/Rhythmbox) are the main
applications I need.

### *Sruthi Chandran (srud)*

**1. Who are you?**

A feminist, a librarian turned Free Software advocate and a Debian Developer.
Part of [Debian Outreach](https://wiki.debian.org/Outreachy) team and
[DebConf](https://www.debconf.org/) Committee.

**2. How did you get introduced to Debian?**

I got introduced to the free software world and Debian through my husband. I
attended many Debian events with him. During one such event, out of curiosity, I
participated in a Debian packaging workshop. Just after that I visited a Tibetan
community in India and they mentioned that there was no proper Tibetan font in
GNU/Linux. Tibetan font was my first package in Debian.

**3. How long have you been into Debian?**

I have been contributing to Debian since 2016 and Debian Developer since 2019.

**4. Are you using Debian in your daily life? If yes, how?**

I haven't used any other distro on my laptop since I got introduced to Debian.

**5. Do you have any suggestions to improve women's participation in Debian?**

I was involved with actively mentoring newcomers to Debian since I started
contributing myself. I specially work towards reducing the gender gap inside the
Debian and Free Software community in general. In my experience, I believe that
visibility of already existing women in the community will encourage more women
to participate. Also I think we should reintroduce mentoring through
debian-women.

### *Tássia Camões Araújo (tassia)*

**1. Who are you?**

Tássia Camões Araújo, a Brazilian living in Canada. I'm a passionate learner who
tries to push myself out of my comfort zone and always find something new to
learn. I also love to mentor people on their learning journey. But I don't
consider myself a typical geek. My challenge has always been to not get
distracted by the next project before I finish the one I have in my hands. That
said, I love being part of a community of geeks and feel empowered by it. I love
Debian for its technical excellence, and it's always reassuring to know that
someone is taking care of the things I don't like or can't do. When I'm not
around computers, one of my favorite things is to feel the wind on my cheeks,
usually while skating or riding a bike; I also love music, and I'm always
singing a melody in my head.

**2. How did you get introduced to Debian?**

As a student, I was privileged to be introduced to FLOSS at the same time I was
introduced to computer programming. My university could not afford to have labs
in the usual proprietary software model, and what seemed like a limitation at
the time turned out to be a great learning opportunity for me and my colleagues.
I joined this student-led initiative to "liberate" our servers and build
LTSP-based labs - where a single powerful computer could power a few dozen
diskless thin clients. How revolutionary it was at the time! And what an
achievement! From students to students, all using Debian. Most of that group
became close friends; I've married one of them, and a few of them also found
their way to Debian.

**3. How long have you been into Debian?**

I first used Debian in 2001, but my first real connection with the community was
attending DebConf 2004. Since then, going to DebConfs has become a habit. It is
that moment in the year when I reconnect with the global community and my
motivation to contribute is boosted. And you know, in 20 years I've seen people
become parents, grandparents, children grow up; we've had our own child and had
the pleasure of introducing him to the community; we've mourned the loss of
friends and healed together. I'd say Debian is like family, but not the kind you
get at random once you're born, Debian is my family by choice.

**4. Are you using Debian in your daily life? If yes, how?**

These days I teach at Vanier College in Montréal. My favorite course to teach is
UNIX, which I have the pleasure of teaching mostly using Debian. I try to
inspire my students to discover Debian and other FLOSS projects, and we are
happy to run a FLOSS club with participation from students, staff and alumni. I
love to see these curious young minds put to the service of FLOSS. It is like
recruiting soldiers for a good battle, and one that can change their lives, as
it certainly did mine.

**5. Do you have any suggestions to improve women's participation in Debian?**

I think the most effective way to inspire other women is to give visibility to
active women in our community. Speaking at conferences, publishing content,
being vocal about what we do so that other women can see us and see themselves
in those positions in the future. It's not easy, and I don't like being in the
spotlight. It took me a long time to get comfortable with public speaking, so I
can understand the struggle of those who don't want to expose themselves. But I
believe that this space of vulnerability can open the way to new connections. It
can inspire trust and ultimately motivate our next generation. It's with this in
mind that I publish these lines.

Another point we can't neglect is that in Debian we work on a volunteer basis,
and this in itself puts us at a great disadvantage. In our societies, women
usually take a heavier load than their partners in terms of caretaking and other
invisible tasks, so it is hard to afford the free time needed to volunteer. This
is one of the reasons why I bring my son to the conferences I attend, and so far
I have received all the support I need to attend DebConfs with him. It is a way
to share the caregiving burden with our community - it takes a village to raise
a child. Besides allowing us to participate, it also serves to show other women
(and men) that you can have a family life and still contribute to Debian.

My feeling is that we are not doing super well in terms of diversity in Debian
at the moment, but that should not discourage us at all. That's the way it is
now, but that doesn't mean it will always be that way. I feel like we go through
cycles. I remember times when we had many more active female contributors, and
I'm confident that we can improve our ratio again in the future. In the
meantime, I just try to keep going, do my part, attract those I can, reassure
those who are too scared to come closer. Debian is a wonderful community, it is
a family, and of course a family cannot do without us, the women.

*These interviews were conducted via email exchanges in October, 2024. Thanks to
all the wonderful women who participated in this interview. We really appreciate
your contributions in Debian and to Free/Libre software.*
