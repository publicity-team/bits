Title: OpenStreetMap migra para Debian 12
Slug: openstreetmap-on-debian
Date: 2024-11-27 14:01
Author: Donald Norwood, Stefano Rivera, Justin B Rye
Translator: Thiago Pezzo (tco), Rogerio Silva, Carlos Henrique Lima Melara
Tags: openstreetmap, debian, interviews, technical, development, ruby, bookworm
Lang: pt-BR
Status: published

Você pode ter visto [este toot](https://en.osm.town/@osm_tech/113050911886181861)
anunciando a migração do OpenStreetMap para o Debian em sua infraestrutura.

> 🚀 Após 18 anos usando o Ubuntu, nós atualizamos os
> servidores [@openstreetmap](https://en.osm.town/@openstreetmap) para o Debian
> 12 (Bookworm). 🌍 [openstreetmap.org](https://openstreetmap.org/) está agora
> mais rápido usando Ruby 3.1. Sigamos para novas aventuras de mapeamento!
> Obrigado à equipe por esta suave transição.
> [#OpenStreetMap](https://en.osm.town/tags/OpenStreetMap)
> [#Debian](https://en.osm.town/tags/Debian) 🤓

Nós conversamos com Grant Slater, o Engenheiro de Operações de Confiabilidade Sênior do Site
da Fundação OpenStreetMap. Grant responde:

**Por que você escolheu o Debian?**

> Há uma grande sobreposição entre os(as) mapeadores(as) OpenStreetMap e a
> comunidade Debian. O Debian também tem uma excelente cobertura de ferramentas
> e utilitários do OpenStreetMap, o que ajudou com a decisão de mudar para o
> Debian.
>
> Os(As) mantenedores(as) de pacotes Debian fazem um excelente trabalho com seus
> pacotes - por exemplo:
> [osm2pgsql](https://packages.debian.org/bookworm/osm2pgsql),
> [osmium-tool](https://packages.debian.org/bookworm/osmium-tool), etc.
>
> Parte do nosso motivo para mudar para o Debian foi chegar mais perto dos(as)
> mantenedores(as) dos pacotes dos quais dependemos. Os(As) mantenedores(as)
> Debian parecem estar fortemente empenhados(as) nos pacotes de software que
> suportam e vemos bugs críticos serem corrigidos.

**O que levou a essa decisão de migrar?**

> O OpenStreetMap.org é executado principalmente em hardware físico real que
> nossa equipe gerencia. Tentamos obter o máximo de desempenho possível de
> nossos sistemas, com alguns serviços sendo particularmente impactados por E/S.
> Tivemos alguns problemas graves de desempenho de E/S com kernels de 6.0 a
> ~6.6 em sistemas com armazenamento NVMe. Isso nos levou para novos kernels da
> linha principal, o que nos levou para o Debian. No Debian 12, podemos
> simplesmente instalar o kernel do backport e os problemas de desempenho são
> resolvidos.

**Como a transição foi gerenciada?**

> Felizmente gerenciamos nossa configuração de servidor quase que completamente
> [via código](https://github.com/openstreetmap/chef). Também usamos
> [Test Kitchen](https://kitchen.ci/) com
> [inspec](https://docs.chef.io/inspec/) para testar este código de
> infraestrutura. Os testes são executados localmente usando
> contêineres Podman ou Docker, mas também são executados como parte do nosso
> pipeline de código git.
>
> Nós adicionamos o Debian como uma plataforma de alvo de testes e corrigimos
> o código da infraestrutura até que todos os testes passassem. As alterações
> necessárias foram relativamente pequenas, em sua maioria nomes de pacotes
> simples ou alterações de nomes de arquivos de configuração.

**Qual foi o seu cronograma de transição?**

> Em agosto de 2024, transferimos os servidores www\.openstreetmap\.org Ruby on
> Rails para o Debian. Ainda não terminamos de mover tudo para o Debian, mas
> vamos atualizar o resto quando for adequado. Alguns sistemas podem esperar
> até o próximo ciclo de atualização de hardware.
>
> Nosso foco é construir uma plataforma estável e confiável para
> mapeadores(as) OpenStreetMap.

**Como ocorreu a transição de outra distribuição Linux para o Debian?**

> Ainda estamos no processo de migração completa entre distribuições
> Linux, mas podemos compartilhar que recentemente movemos nossos servidores
> frontend para o Debian 12 (do Ubuntu 22.04), o que elevou a versão Ruby de 3.0
> para 3.1, o que também nos permitiu atualizar a versão do Ruby on Rails que
> usamos para o www\.openstreetmap\.org.
>
> Também mudamos nosso código do chef, para gerenciar as interfaces de rede, do
> netplan (padrão no Ubuntu, feito pela Canonical) para usar
> diretamente o systemd-networkd para gerenciar as interfaces de rede,
> permitindo a comunalidade entre como gerenciamos as interfaces no Ubuntu e
> nossos próximos sistemas Debian. Ao longo dos anos, padronizamos nossa
> configuração de rede para usar interfaces com ligação 802.3ad para
> redundância, com VLANs para tráfego de segmento; essa configuração funcionou
> bem com o systemd-networkd.
>
> Usamos [netboot.xyz](https://netboot.xyz/) para instaladores de SO de
> inicialização de rede PXE para nossos sistemas, e usamos IPMI para o
> gerenciamento fora da banda. Reinstalamos remotamente um servidor de testes
> para o Debian 12 e corrigimos alguns problemas menores que escaparam de
> nossos testes do chef. Ficamos agradavelmente surpresos(as) com o quão suave
> foi a migração para o Debian.
>
> Em alguns casos limitados, usamos o Debian Backports para alguns pacotes para
> os quais precisávamos de recursos mais recentes. Os(As) mantenedores(as) de
> pacotes Debian são fantásticos(as).
>
> O que definitivamente nos ajudou é que nosso código é
> livre/libre/free/open-source, com a maior parte do núcleo do software
> OpenStreetMap como osm2pgsql já no Debian e bem empacotado.
>
> Em alguns casos, executamos patches de pré-lançamento ou personalizados do
> software OpenStreetMap; com o Ubuntu, usamos o Personal Package Archives (PPA)
> do launchpad.net para criar e hospedar repositórios deb para esses pacotes
> personalizados. Inicialmente ficamos perplexos(as) com a miríade de opções no
> Debian (veja [esta lista](https://wiki.debian.org/DebianRepository/Setup) -
> _uau!_), mas recebemos algumas orientações úteis de um(a) contribuidor(a)
> Debian e agora gerenciamos nosso próprio repositório deb usando aptly. Por
> enquanto estamos construindo pacotes deb localmente e enviando para o aptly;
> idealmente, no futuro gostaríamos de substituir por um pipeline em git para
> construir os pacotes personalizados.

**Obrigado pelo seu tempo disponibilizado em compartilhar sua experiência
conosco.**

> Gratidão a todas as pessoas maravilhosas que fazem o Debian!

***

Estamos muito felizes em compartilhar este caso de uso que demonstra nosso
compromisso com a estabilidade, o desenvolvimento e o suporte de longo prazo. O
Debian oferece aos(às) usuários(as), empresas e organizações a capacidade de
planejar, examinar, desenvolver e manter em seu próprio ritmo usando
uma distribuição Linux estável com desenvolvedores(as) responsivos(as).

Sua organização usa Debian de alguma forma? Gostaríamos muito de ouvir
sobre isso e sobre seu uso do “Sistema Operacional Universal”. Alcance-nos em
[Press@debian.org](mailto:press@debian.org) - ficaríamos felizes em adicionar
sua organização em nossa página ['Quem está usando o
Debian?'](https://www.debian.org/users/) e em compartilhar sua história!

## Sobre o Debian

O Projeto Debian é uma associação de pessoas que têm como causa comum criar
um sistema operacional livre. O sistema operacional que criamos é chamado
[Debian](https://www.debian.org/). Instaladores e imagens, como sistemas "live",
instaladores off-line para sistemas sem conexão de rede, instaladores para
outras arquiteturas de CPU, ou instâncias em nuvem, podem ser encontrados em
[Obtendo o Debian](https://www.debian.org/distrib/).
