Title: « Ceratopsian » : le prochain thème par défaut pour Debian 13
Slug: ceratopsian-will-be-the-default-theme-for-debian-13
Date: 2024-12-04 13:30
Author: Jonathan Carter
Artist: Elise Couper
Tags: trixie, artwork
Lang: fr
Translator: Jean-Pierre Giraud
Status: published

Le thème ["Ceratopsian"][1] d'Elise Couper a été choisi comme thème par défaut
de Debian 13 « Trixie ». Ce thème s'inspire par la collerette de Trixie, le
personnage de Toy Story, et a aussi été influencé par le thème
« futurePrototype » créé par Alex Makas.

[![Fond d'écran Ceratopsian. Cliquez pour voir la totalité du thème proposé](|static|/images/ceratopsian_wallpaper.png)][1]

[![Bannière du site web de Debian. Cliquez pour voir la totalité du thème proposé](|static|/images/ceratopsian_website_banner.png)][1]

Après [l'appel à propositions pour le thème de Trixie][2] lancé par l'équipe
Debian Desktop, un total de [six offres][3] ont été soumises. Le vote pour le
thème a été ouvert au public et 2817 réponses classant les différentes
propositions ont été reçues et c'est «  Ceratopsian » qui a été classé premier.

Nous remercions tous les graphistes qui ont participé et proposé leur excellent
travail sous la forme de fonds d'écran et de thème graphique pour Debian 13.

Félicitations Elise, et merci beaucoup pour ta contribution à Debian !

[1]: https://wiki.debian.org/DebianArt/Themes/Ceratopsian
[2]: https://lists.debian.org/debian-devel-announce/2024/06/msg00003.html
[3]: https://wiki.debian.org/DebianDesktop/Artwork/Trixie
