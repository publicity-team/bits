Title: "Ceratopsian" will be the default theme for Debian 13
Slug: ceratopsian-will-be-the-default-theme-for-debian-13
Date: 2024-12-04 13:30
Author: Jonathan Carter
Artist: Elise Couper
Tags: trixie, artwork
Status: published

The theme ["Ceratopsian"][1] by Elise Couper has been selected as the default
theme for Debian 13 "trixie". The theme is inspired by Trixie's (the fictional
character from Toy Story) frill and is also influenced by a previously used
theme called "futurePrototype" by Alex Makas.

[![Ceratopsian wallpaper. Click to see the whole theme proposal](|static|/images/ceratopsian_wallpaper.png)][1]

[![Ceratopsian Website banner. Click to see the whole theme proposal](|static|/images/ceratopsian_website_banner.png)][1]

After the Debian Desktop Team made the [call for proposing themes][2], a total
of [six choices][3] were submitted. The desktop artwork poll was open to the
public, and we received 2817 responses ranking the different choices, of which
Ceratopsian has been ranked as the winner among them.

We'd like to thank all the designers that have participated and have submitted
their excellent work in the form of wallpapers and artwork for Debian 13.

Congratulations, Elise, and thank you very much for your contribution to
Debian!

[1]: https://wiki.debian.org/DebianArt/Themes/Ceratopsian
[2]: https://lists.debian.org/debian-devel-announce/2024/06/msg00003.html
[3]: https://wiki.debian.org/DebianDesktop/Artwork/Trixie
