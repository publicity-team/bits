Title: New Debian Developers and Maintainers ($Month and $Month (or $Month) YYYY)
Slug: new-developers-YYYY-MM
Date: YYYY-MM-DD HH:MM
Author: $You
Tags: project developers
Lang: en
Status: published|draft

The following contributors got their Debian Developer accounts in the last two months:

* $DD-FullName ($DD-AccountName)
* $DD-FullName ($DD-AccountName)
* $DD-FullName ($DD-AccountName)

The following contributors were added as Debian Maintainers in the last two months:

* $Contributor Name
* $Contributor Name
* $Contributor Name

Congratulations!
